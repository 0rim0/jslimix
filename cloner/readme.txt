*usage

cloner(elem, opt)
cloner.add(elem)


*arguments and type

elem:
  element(s) or selector
opt:
  template:
    element or selector
  parent:
    element or selector
  add_button:
    element(s) or selector
  delete_button:
    selector of template
  onbeforeadd
  onbeforedelete
  onadd
  ondelete
    function


*recommend css

.cloner-origin{display: none !important;}
