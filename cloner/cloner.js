!(function() {
	var wmap = new WeakMap()
	function init(elem, opt) {
		var elems = resolveElements(elem)

		elems.forEach(function(root) {
			var origin_selector
			var origin

			if (opt.template instanceof HTMLElement) {
				origin_selector = null
				origin = opt.template
			} else {
				origin_selector = opt.template || ".cloner-origin"
				origin = document.querySelector(origin_selector)
			}

			if (!origin) {
				throw new Error("Template not found.")
			}

			var parent = resolveElement(opt.parent, root) || origin.parentElement
			origin.parentElement.removeChild(origin)

			// if template is HTMLTemplateElement, use inner element.
			if (origin.content instanceof DocumentFragment) {
				if (origin.content.children.length > 1) {
					origin = document.createElement("div")
					origin.appendChild(origin.content)
				} else {
					origin = origin.content.firstElementChild
				}
			}

			origin.classList.remove("cloner-origin")
			origin.classList.add("cloner-replica")

			var add_elems = resolveElements(opt.add_button)
			add_elems.forEach(function(e) {
				e.addEventListener("click", generate.bind(null, root), false)
			})

			var delete_selector = ".cloner-delete"
			if (typeof opt.delete_button === "string") {
				delete_selector = opt.delete_button
			}
			root.addEventListener("click", remove.bind(null, delete_selector), false)

			wmap.set(root, {
				origin: origin,
				parent: parent,
				onbeforeadd: opt.onbeforeadd,
				onadd: opt.onadd,
				onbeforedelete: opt.onbeforedelete,
				ondelete: opt.ondelete,
			})
		})
	}

	function resolveElements(elem, root) {
		if (elem instanceof HTMLElement) {
			return [elem]
		}
		if (Array.isArray(elem)) {
			return elem
				.map(function(e) {
					return resolveElements(e, root)
				})
				.reduce(function(a, b) {
					return a.concat(b)
				})
		}
		if (typeof elem === "string") {
			var selector = elem
			return [].filter.call(document.querySelectorAll(selector), function(e) {
				return !root || root === e || root.contains(e)
			})
		}

		return []
	}

	function resolveElement(elem, root) {
		if (elem instanceof HTMLElement) {
			return elem
		}

		if (typeof elem === "string") {
			var selector = elem
			if (root) {
				return [].find.call(document.querySelectorAll(selector), function(e) {
					return root === e || root.contains(e)
				})
			} else {
				return document.querySelector(selector)
			}
		}

		return null
	}

	// for IE11
	function domClosest(elem, selector) {
		if (elem.closest) {
			return elem.closest(selector)
		} else {
			for (; elem; elem = elem.parentElement) {
				if (domMatches(elem, selector)) {
					return elem
				}
			}
			return null
		}
	}

	// for IE11
	function domMatches(elem, selector) {
		if (elem.matches) {
			return elem.matches(selector)
		} else {
			return [].some.call(document.querySelectorAll(selector), function(e) {
				return e === elem
			})
		}
	}

	function generate(elem, eve) {
		var elems = resolveElements(elem)

		return elems
			.map(function(e) {
				var data = wmap.get(e)
				if (data) {
					var replica = data.origin.cloneNode(true)

					// on before event
					var cancelable = { element: replica, cancel: false }
					data.onbeforeadd && data.onbeforeadd(cancelable)
					if (cancelable.cancel) {
						return null
					}

					data.parent.appendChild(replica)

					// on after event
					data.onadd && data.onadd({ element: replica })
					return replica
				}
			})
			.filter(function(e) {
				return e
			})
	}

	function remove(delete_selector, eve) {
		if (domMatches(eve.target, delete_selector + "," + delete_selector + " *")) {
			var replica = domClosest(eve.target, ".cloner-replica")
			if (!replica) {
				return
			}

			var data = wmap.get(eve.currentTarget)

			// on before event
			var cancelable = { element: replica, cancel: false }
			data.onbeforedelete && data.onbeforedelete(cancelable)
			if (cancelable.cancel) {
				return
			}

			replica.parentElement.removeChild(replica)

			// on after event
			data.ondelete && data.ondelete({ element: replica })
		}
	}

	// export
	init.data_map = wmap
	init.add = generate
	window.cloner = init
})()
