てぃりー
tily: utility

他に使うような便利ツール
DOM 依存しない Types

ただの関数は ⇨ sifon
プロトタイプ拡張 ⇨ exon


---

おて
OTE: ObjectTreeEntry

木構造のエントリ
値と子要素(OTEインスタンス)を持てる
階層構造を通常のオブジェクトで作ると構造の一部なのかデータとしてのオブジェクトなのか判断が難しくなる
構造部分を OTE インスタンスでつくる
全エントリごとの関数実行や通常オブジェクト化、変換対応のクローンなどをサポート

データ的には
Symbol の ObjectEntryTree.entry_value をキーにしたプロパティがエントリの値
文字列がキーの通常プロパティが子要素

子要素と値の両方を持つエントリも作れる

****
const ote = new ObjectTreeEntry
const e = ote.create("key1")
e.set("a")
ote.createRecursive("a.b.c", 30)
ote.toPlainObject()
// { ke1: "a",
//   a: { b: { c: 30 } }
// }
****
