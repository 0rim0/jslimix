// めりー
// mery: memory entry
!(function() {
	const info_symbol = Symbol("info")

	function Entry(name, parent, value) {
		if (!name || !(typeof name === "string") || (parent !== null && !Entry.isEntry(parent))) {
			throw new Error("invalid arguments")
		}

		this[info_symbol] = { name, parent, value }
	}

	Object.assign(Entry.prototype, {
		createFile(name, value) {
			if (this.isFile()) throw new Error("Cannot create a file in a file.")
			if (name in this) throw new Error(name + " already exists.")
			this[name] = new Entry(name, this, value)
		},
		createDirectory(name) {
			if (this.isFile()) throw new Error("Cannot create a directory in a file.")
			if (name in this) throw new Error(name + " already exists.")
			this[name] = new Entry(name, this)
		},
		getEntryNames() {
			return this.isDirectory() && Object.keys(this)
		},
		getDirectoryNames() {
			return this.isDirectory() && this.getEntryNames().filter(e => e.isDirectory())
		},
		getFileNames() {
			return this.isDirectory() && this.getEntryNames().filter(e => e.isFile())
		},
		getSelfName() {
			return this[info_symbol].name
		},
		getParent() {
			return this[info_symbol].parent
		},
		getPath() {
			const info = this[info_symbol]
			const parent = info.parent ? info.parent.getPath() : ""
			const separator = this.isDirectory() ? "/" : ""
			return parent + info.name + separator
		},
		isDirectory() {
			return this[info_symbol].value === undefined
		},
		isFile() {
			return !this.isDirectory()
		},
		write(value) {
			if (this.isDirectory()) throw new Error("Cannot write data to a directory.")
			this[info_symbol].value = value
		},
		read() {
			if (this.isDirectory()) throw new Error("Cannot read data from a directory.")
			return this[info_symbol].value
		},
		forEach(fn) {
			this.getEntryNames().forEach(name => {
				const current_entry = this[name]
				fn(current_entry, name, this)

				if (current_entry.isDirectory()) {
					current_entry.forEach(fn)
				}
			})
		},
	})

	Entry.isEntry = may_entry => {
		return may_entry && info_symbol in may_entry
	}

	Entry.isFile = may_file => {
		return Entry.isEntry(may_file) && may_file.isFile()
	}

	Entry.toJSON = (entry, serializer) => {
		if (entry.isFile()) throw new Error("Only directory can convert to JSON.")

		const file_key =
			"FILEKEY::" +
			Math.random()
				.toString(36)
				.substr(2)

		entry.forEach(e => {
			if (e.isFile()) {
				e[file_key] = via(e.read(), serializer)
			}
		})

		const result = JSON.stringify({
			file_key,
			entry,
		})

		entry.forEach(e => {
			if (e.isFile()) {
				delete e[file_key]
			}
		})

		return result
	}

	Entry.fromJSON = (json, deserializer) => {
		const { file_key, entry: entry_tree } = JSON.parse(json)

		const entry = Entry.createTree()

		buildTree(entry, entry_tree, file_key, deserializer)

		return entry
	}

	function buildTree(entry, entry_tree, file_key, deserializer) {
		Object.keys(entry_tree).forEach(name => {
			if (file_key in entry_tree[name]) {
				const value = via(entry_tree[name][file_key], deserializer)
				entry[name] = new Entry(name, entry, value)
			} else {
				entry[name] = new Entry(name, entry)
				buildTree(entry[name], entry_tree[name], file_key, deserializer)
			}
		})
	}

	function via(value, ...fns) {
		return fns.reduce((acc, fn) => (fn ? fn(acc) : acc), value)
	}

	Entry.info_symbol = info_symbol

	Entry.createTree = () => new Entry("Root:", null)

	window.Entry = Entry
})()
