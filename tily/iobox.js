!(function() {
	function IObox(opt) {
		opt = opt || {}
		this.async = !!opt.async
		this.registered = []
	}
	IObox.prototype.register = function(name, input_fn) {
		if (!input_fn) {
			input_fn = name
			name = ""
		}

		name = (name || "").toString()
		const reg_item = { name, fn: input_fn }

		const send = (data, target, callback) => {
			// except registered itself
			const filterRemoveItself = item => item !== reg_item

			if (target) {
				target = { and: [target, filterRemoveItself] }
			} else {
				target = filterRemoveItself
			}

			this.send(data, target, callback)
		}

		reg_item.send = send
		this.registered.push(reg_item)

		return send
	}

	IObox.prototype.send = function(data, target_filter, callback) {
		this.registered.forEach(reg_item => {
			if (checkTarget(target_filter, reg_item)) {
				if (this.async) {
					setImmediate(_ => {
						reg_item.fn(data, callback || function() {})
					})
				} else {
					reg_item.fn(data, callback || function() {})
				}
			}
		})

		function checkTarget(filter, item) {
			if (!filter) return true
			if (typeof filter === "string") {
				return filter === item.name
			}
			if (typeof filter === "function") {
				return !!filter(item)
			}
			if (Array.isArray(filter)) {
				return filter.some(e => checkTarget(e, item))
			}
			if (typeof filter === "object") {
				if (Array.isArray(filter.and)) {
					return filter.and.every(e => checkTarget(e, item))
				} else if (Array.isArray(filter.or)) {
					return checkTarget(or, item)
				}
			}
			return false
		}
	}

	function setImmediate(fn) {
		setTimeout(fn, 4)
	}

	window.IObox = IObox
})()

////////////////

var iobox = new IObox()
var send = iobox.register("namae", function(data, callback) {
	console.log(data, callback)
	callback(data + 1)
})

iobox.send(100)
// 100 function(){}

send(100)
// undefined  <-- No self registered function is called.

var send2 = iobox.register("name", function(data, callback) {
	console.log("!!!", data, callback)
})

send(100)
// !!! 100 function (){}

iobox.send(100)
// 100 function (){}
// !!! 100 function (){}

send2(100)
// 100 function (){}

iobox.send(100, "namae")
// 100 function (){}

send(100, "namae")
// undefined <-- no function called

iobox.send(100, "namae", function(value) {
	console.log("callback called: ", value)
})
// 100 function(value){console.log("callback called:", value)})
// callback called: 101

var iobox = new IObox()

var s1 = iobox.register(e => {
	console.log(e)
	s1(e + 1)
})

var s2 = iobox.register(e => {
	console.log(e)
	s2(e + 1)
})

s1(1)
// infinite loop, ... stackoverflow

var iobox = new IObox({ async: true })

var s1 = iobox.register(e => {
	console.log(e)
	s1(e + 1)
})

var s2 = iobox.register(e => {
	console.log(e)
	s2(e + 1)
})

s1(1)
// it is async, you can control browser.

// sample case
iobox.send("http://aaaaa.com/bcd.json", "get-json", function(data) {
	console.log(data)
})

iobox.send("a.b.c", "get-from-db", function(data) {
	console.log(data)
})

// sample case: between frames

var iframe = document.createElement("iframe")
document.body.append(iframe)
var iframe_script = document.createElement("script")
iframe_script.innerHTML = `
	function init(iobox){
		var send = iobox.register(data => console.log("frame: ", data))
		send("frame initialized")
	}
	console.log("frame script - loaded")
`
iframe.contentDocument.head.append(iframe_script)

var iobox = new IObox()
var send = iobox.register(data => console.log("top: ", data))
iframe.contentWindow.init(iobox)
