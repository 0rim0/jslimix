function ObjectTreeEntry(value) {
	if (arguments.length > 0) {
		this.set(value)
	} else {
		this.set(this.constructor.undefined)
	}
}

Object.assign(ObjectTreeEntry, {
	path_separator: ".",
	entry_value: Symbol("ObjectTreeEntry.entry_value"),
	undefined: Symbol("ObjectTreeEntry.undefined"),
	from(obj) {
		const result = new this()
		for (const [key, value] of Object.entries(obj)) {
			if (value instanceof this) {
				result[key] = new this(value.valueOf())
			} else {
				result[key] = new this(value)
			}
		}
		return result
	},
})

Object.assign(ObjectTreeEntry.prototype, {
	get(path) {
		const ctor = this.constructor
		if (path == null) {
			return this.valueOf()
		}
		if (typeof path === "string") {
			path = path.split(ctor.path_separator)
		}
		let entry = this
		for (const name of path) {
			entry = entry[String(name)]
			if (!(entry instanceof ctor)) return ctor.undefined
		}
		return entry.valueOf()
	},
	route(path, allow_halfway = false) {
		const ctor = this.constructor
		if (typeof path === "string") {
			path = path.split(ctor.path_separator)
		}
		const result = []
		let entry = this
		for (const name of path) {
			entry = entry[String(name)]
			if (entry instanceof ctor) {
				result.push(entry)
			} else {
				if (allow_halfway) {
					return result
				} else {
					throw new Error(`Key '${name}' is not found`)
				}
			}
		}
		return result
	},
	clone(convert = x => x) {
		const map = new Map()
		const ctor = this.constructor
		return (function recur(ientry, path) {
			if (map.has(ientry)) return map.get(ientry)
			const oentry = new ctor()
			oentry.set(convert(ientry.valueOf(), path.slice()))
			for (const [key, value] of Object.entries(ientry)) {
				if (value instanceof ctor) {
					oentry[key] = recur(value, [...path, key])
				}
			}
			map.set(ientry, oentry)
			return oentry
		})(this, [])
	},
	toPlainObject(convert = x => x, throw_if_unsafe = false) {
		const map = new Map()
		const ctor = this.constructor
		return (function recur(entry, path) {
			if (map.has(entry)) return map.get(entry)
			const obj = {}
			for (const [key, value] of Object.entries(entry)) {
				if (value instanceof ctor) {
					const tmp = recur(value, [...path, key])
					if (tmp !== ctor.undefined) {
						obj[key] = tmp
					}
				}
			}
			// Has no actual children (empty one is ignored)
			if (Object.keys(obj).length === 0) {
				if (entry.hasValue()) {
					return convert(entry.valueOf(), path.slice())
				} else {
					return ctor.undefined
				}
			}
			// Has actual children and value, it is unsafe for plain object
			if (entry.hasValue() && throw_if_unsafe) {
				throw new Error("A entry has both children and value.")
			}
			// Regard obj as array if all keys are integer value
			const result = Object.keys(obj).every(e => +e === ~~e) ? Object.values(obj) : obj
			map.set(entry, result)
			return result
		})(this, [])
	},
	walk(action, filter) {
		const set = new Set()
		const ctor = this.constructor
		!(function recur(entry, path) {
			if (set.has(entry)) return
			set.add(entry)
			for (const [key, value] of Object.entries(entry)) {
				if (value instanceof ctor) {
					recur(value, [...path, key])
				}
			}
			let matched = true
			if ("is_end" in filter) {
				const is_end = !entry.hasChildren()
				matched &= (filter.is_end && is_end) || (!filter.is_end && !is_end)
			}
			if ("has_value" in filter) {
				const has_value = entry.hasValue()
				matched &= (filter.has_value && has_value) || (!filter.has_value && !has_value)
			}
			if (matched) {
				action(entry.valueOf(), path.slice())
			}
		})(this, [])
	},
	getNextIndex() {
		return (
			Object.keys(this)
				.filter(k => +k === ~~k)
				.reduce((a, k) => Math.max(a, ~~k), -1) + 1
		)
	},
	isEmpty() {
		return !this.hasValue() && !this.hasChildren()
	},
	hasChildren() {
		return Object.values(this).some(e => e instanceof this.constructor)
	},
	hasValue() {
		return this.valueOf() !== this.constructor.undefined
	},
	hasEntry(path, include_empty_entry = true) {
		const ctor = this.constructor
		if (typeof path === "string") {
			path = path.split(ctor.path_separator)
		}
		let entry = this
		for (const name of path) {
			entry = entry[String(name)]
			if (!(entry instanceof ctor)) return false
		}
		return !!include_empty_entry || !entry.isEmpty()
	},
	create(key, value, onduplicate = true) {
		const ctor = this.constructor
		if (arguments.length < 2) {
			value = ctor.undefined
		}
		// Regard empty entry as not existing
		const haskey = this[key] instanceof ctor && !this[key].isEmpty()
		if (!haskey || onduplicate === true) {
			return (this[key] = new ctor(value))
		} else if (haskey && onduplicate === "throw") {
			throw new Error(`Key '${key}' already exists in entry.`)
		} else if (haskey && typeof onduplicate === "function") {
			const current = this[key].valueOf()
			return (this[key] = new ctor(onduplicate(current, value, key, this)))
		}
	},
	createRecursive(path, value, onduplicate = true) {
		const ctor = this.constructor
		if (typeof path === "string") {
			path = path.split(ctor.path_separator)
		}
		if (arguments.length < 2) {
			value = ctor.undefined
		}
		let entry = this
		let haskey = true
		for (const name of path) {
			const str_name = String(name)
			if (entry[str_name] instanceof ctor) {
				entry = entry[str_name]
			} else {
				entry[str_name] = new ctor()
				entry = entry[str_name]
				haskey = false
			}
		}
		// Regard empty entry as not existing
		haskey &= !entry.isEmpty()
		if (!haskey || onduplicate === true) {
			return entry.set(value)
		} else if (haskey && onduplicate === "throw") {
			throw new Error(`Key '${path.join(ctor.path_separator)}' already exists in entry.`)
		} else if (haskey && typeof onduplicate === "function") {
			const current = entry.valueOf()
			return entry.set(onduplicate(current, value, path, entry))
		}
	},
	set(value) {
		return (this[this.constructor.entry_value] = value)
	},
	valueOf() {
		return this[this.constructor.entry_value]
	},
})
