!(function() {
	function overload(overloads, default_func, opt = {}) {
		if (!overloads || (!Array.isArray(overloads) && !overloads.overloads)) {
			throw new Error("'overloads' must be a array or object which has property 'overloads'")
		}
		const funcpool = overloads.funcpool
		overloads = overloads.overloads || overloads
		const mode = opt.mode || "single"
		const before = opt.before
		const after = opt.after

		const run = (...args) => {
			if (typeof before === "function") {
				args = before(args)
			}

			let result
			let has_matched = false
			if (mode === "single") {
				has_matched = overloads.some(e => {
					if (checkMatched(e.type, args)) {
						result = callOverload(e, funcpool, args)
						return true
					}
				})
			} else {
				result = overloads.map(e => {
					if (checkMatched(e.type, args)) {
						has_matched = true
						return callOverload(e, funcpool, args)
					}
					return null
				})
			}

			if (!has_matched && default_func) {
				if (default_func.hasOwnProperty("func")) {
					result = callOverload(default_func)
				} else if (typeof default_func === "function") {
					result = default_func(...args)
				} else {
					result = default_func
				}

				if (mode !== "single") {
					result = [result]
				}
			}

			if (typeof after === "function") {
				result = after(result)
			}

			return result
		}

		run.funcpool = funcpool
		return run
	}

	function callOverload(ovl_item, funcpool, args) {
		const bind = ovl_item.this || (typeof ovl_item.lazy_this === "function" && ovl_item.lazy_this()) || null

		if (typeof ovl_item.type === "object" && !Array.isArray(ovl_item.type)) {
			args = [args[0]]
		}

		if (ovl_item.func) {
			return ovl_item.func.apply(bind, args)
		} else if (ovl_item.func_name && ovl_item.func_name in funcpool) {
			return funcpool[ovl_item.func_name](...args)
		} else {
			throw new Error("function not found. 'overload item' must have func or func_name property.")
			console.error(ovl_item)
		}
	}

	function checkMatched(type, args) {
		if (typeof type === "function") {
			return !!type(...args)
		} else if (Array.isArray(type)) {
			return type.every((cond, i) => {
				return checkItem(cond, args[i])
			})
		} else if (typeof type === "object") {
			const arg_obj = args[0]
			if (!arg_obj || typeof arg_obj !== "object") return false
			return Object.keys(type).every(key => {
				return checkItem(type[key], arg_obj[key])
			})
		} else {
			throw new Error("'type' must be a function or array")
		}

		function checkItem(cond, value) {
			if (typeof cond === "function") {
				return !!cond(value)
			} else if (typeof cond === "string") {
				if (cond.trim() === "*") return true
				return cond.split("|").some(e => {
					const [, type_name, question] = e.trim().match(/^(.*?)(\??)$/)
					if (question === "?" && value == null) return true
					if (value == null) return false
					if (typeof value === type_name) return true
					if (value.constructor.name === type_name) return true
					return false
				})
			} else {
				throw new Error("'type condition' must be a function or string")
			}
		}
	}

	window.soad = overload
})()
