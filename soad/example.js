function isInt32(n) {
	return n === ~~n
}
const obj = { a: 1 }

const f1 = soad(
	[
		{
			type: ["number", "number", "number", "number"],
			this: obj,
			func(a, b, c, d) {
				console.log(1, this, a, b, c, d)
			},
		},
		{
			type: ["string", "Date", "Array"],
			func: (a, b, c) => {
				console.log(2, a, b, c)
			},
		},
		{
			type: [isInt32, e => e > 100],
			func: (a, b) => {
				console.log(3, a, b)
			},
		},
		{
			type: (a, b, c) => a + b + c < 1,
			func: (a, b, c) => {
				console.log(4, a, b, c)
			},
		},
	],
	(...args) => console.error("No matched function", args)
)

f1(1, 2, 3, 4)
// 1 {a: 1} 1 2 3 4

f1("abc", new Date(), [100, 200])
// 2 "abc" 2017-03-25T06:48:20.127Z [ 100, 200 ]

f1(1, 2)
// No matched function [1, 2]

f1(1, 200)
// 3 1 200

f1(-10, -20, 30)
// 4 -10 -20 30

f1(10, 20, 30)
// No matched function [10, 20, 30]

////////////////////////////////////////////////////////////////////////////////

const f2 = soad(
	{
		overloads: [
			{
				type: ["Array", "number"],
				func_name: "splitAt",
			},
			{
				type: ["Array"],
				func_name: "splitHalf",
			},
		],
		funcpool: {
			splitAt(arr, n) {
				return [arr.slice(0, n), arr.slice(n)]
			},
			splitHalf(arr) {
				return this.splitAt(arr, Math.ceil(arr.length / 2))
			},
		},
	},
	(...args) => console.error("No matched function", args)
)

console.log(f2([1, 2, 3, 4, 5, 6], 2))
// [ [ 1, 2 ], [ 3, 4, 5, 6 ] ]

console.log(f2([1, 2, 3, 4, 5]))
// [ [ 1, 2, 3 ], [ 4, 5 ] ]

////////////////////////////////////////////////////////////////////////////////

const f3 = soad(
	[
		{
			type: ["number", "number", "number", "number"],
			this: obj,
			func(a, b, c, d) {
				console.log(1, this, a, b, c, d)
				return "NNNN"
			},
		},
		{
			type: ["string", "Date", "Array"],
			func: (a, b, c) => {
				console.log(2, a, b, c)
				return "SDA"
			},
		},
		{
			type: [isInt32, e => e > 100],
			func: (a, b) => {
				console.log(3, a, b)
				return "IF"
			},
		},
		{
			type: (a, b, c) => a + b + c < 1,
			func: (a, b, c) => {
				console.log(4, a, b, c)
				return "XXX<1"
			},
		},
	],
	(...args) => console.error("No matched function", args),
	{
		before: args => {
			console.log(args)
			return args
		},
		after: args => {
			console.log(args)
			return args
		},
		mode: "multi",
	}
)

f3(-100, 200, -100, 200)
// [ -100, 200, -100, 200]          <-- before callback
// 1 {a: 1} -100 200 -100 200       <-- matched function
// 3 -100 200                       <-- matched function
// 4 -100 200 -100                  <-- matched function
// [ "NNNN", null, "IF", "XXX<1" ]  <-- after callback
// [ "NNNN", null, "IF", "XXX<1" ]  <-- return value

f3(100, 200)
// [ 100, 200]                 <-- before callback
// 3 100 200                   <-- matched function
// [ null, null, "IF", null ]  <-- after callback
// [ null, null, "IF", null ]  <-- return value

////////////////////////////////////////////////////////////////////////////////

const f4 = soad(
	{
		overloads: [
			{
				type: ["Array", "number", "string"],
				func_name: "fn",
			},
			{
				type: { num: "number", arr: "Array", str: "string" },
				func_name: "fn2",
			},
		],
		funcpool: {
			fn(arr, num, str) {
				return arr[num] === str
			},
			fn2({ num, arr, str }) {
				return this.fn(arr, num, str)
			},
		},
	},
	(...args) => console.error("No matched function", args)
)

console.log(f4(["a", "b", "c", "d"], 1, "b"))
// true

console.log(f4(["a", "b", "c", "d"], 3, "d"))
// true

console.log(f4(["a", "b", "c", "d"], 3, "b"))
// false

console.log(f4({ num: 1, str: "def", arr: ["abc", "def", "ghi"] }))
// true

console.log(f4({ num: 1, str: "xyz", arr: ["abc", "def", "ghi"] }))
// false

////////////////////////////////////////////////////////////////////////////////

const f5 = soad(
	[
		{
			type: ["number?", "boolean"],
			func(a, b) {
				console.log(1, a, b)
			},
		},
		{
			type: ["string|Date"],
			func: d => {
				console.log(2, new Date(d))
			},
		},
	],
	(...args) => console.error("No matched function", args)
)

f5(1, true)
// 1 1 true

f5(null, true)
// 1 null true

f5(undefined, true)
// 1 undefined true

f5("2020-12-31")
// 2 2020-12-30T15:00:00.000Z

f5(new Date(2020, 11, 31))
// 2 2020-12-30T15:00:00.000Z

////////////////////////////////////////////////////////////////////////////////

const same = a => b => a === b
const f6 = soad(
	[
		{
			type: [same("abc")],
			func(_type_, data) {
				return data[0]
			},
		},
		{
			type: [same("def")],
			func(_type_, data) {
				return data[1]
			},
		},
		{
			type: [same("ghi")],
			func(_type_, data) {
				return data[2]
			},
		},
	],
	(...args) => console.error("No matched function", args)
)

console.log(f6("abc", [1, 2, 3]))
// 1

console.log(f6("def", [1, 2, 3]))
// 2

console.log(f6("ghi", [1, 2, 3]))
// 3
