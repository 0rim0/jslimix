そーど
soad: scripting overload

組み込まれた機能じゃなく実行時にスクリプトで処理する overload


npm の ovl パッケージと同じもの

//////////////

soad(
	[
		// 配列の要素ひとつひとつがオーバーロードの関数の定義
		// type と func は必須。func は funcpool があるときは funcname で代替可能
		{
			// 引数の条件を指定。これにマッチすると func が呼び出される
			type: ["number"],
			
			// type にマッチしたときに呼び出される関数
			func: e => {"引数は関数実行時の引数。返り値は after 処理がなければそのまま関数の返り値になる"},
			
			// func に bind されるオブジェクト。 this を変えたいときに
			this: {},
			
			// 遅延して this 設定。必要なときに関数を実行するので実行時に取得した値を設定できる
			// this があるならそっちが優先される
			lazy_this: () => {"this にしたいオブジェクトを返す"}
			
			// funcpool があるときに funcpool のキーを設定。その関数が func として呼び出される
			func_name: "",
		},
	],
	
	// "マッチしないときに実行される関数"
	e => {"関数の引数がそのまま引数で返り値もそのまま関数の返り値になる"},
	{
		// マッチする関数検索の前処理。引数をカスタマイズ可能。値はそのまま通してログだけすることも可能
		before: e => {"共通の前処理済みの値を返す"},
		
		// 関数実行後の後処理。返す値をカスタマイズ可能。値はそのまま通してログだけすることも可能
		after: e => {"共通の後処理済みの値を返す"},
		
		// single: 普通のオーバーロード。最初にマッチした一つが実行される
		// multi: マッチしたものすべてが実行される。すべての返り値を配列で返す
		mode: "single",
	}
)


soad({
	// 第一引数が配列のときのものと同じ
	overloads: [],
	
	// オーバーロード用関数置き場。複数の type が同じものを参照したり
	// this.xxx(a) 形式で別関数を参照したいときにつかう
	funcpool: {}
}, e => {})


// type と引数のマッチング例

	type: ["number", "number"]
	arguments: [1, 2]
	result: yes

	type: ["number", "string"]
	arguments: [1, 2]
	result: no

	type: ["number"]
	arguments: [1, 2]
	result: yes

	type: ["number", function(x){ return x === 2 }]
	arguments: [1, 2]
	result: yes

	type: ["number", function(x){ return x === 1 }]
	arguments: [1, 2]
	result: no

	type: ["number", "number?"]
	arguments: [1, null]
	result: yes

	type: ["number", "number"]
	arguments: [1, null]
	result: no

	type: ["number", "number|string"]
	arguments: [1, 1]
	result: yes

	type: ["number", "number|string"]
	arguments: [1, "a"]
	result: yes

	type: function(a, b){return a === 1 && b === 2}
	arguments: [1, 2]
	result: yes

	type: {a: "number", b: "string"}
	arguments: [{a: 1, b: "a"}]
	result: yes

	type: {a: "number", b: "string"}
	arguments: [{a: 1, b: 2}]
	result: no

// before と after

こういう使い方をするケースがあったので前処理と後処理も overload 定義に含められるようにしてる

var func = function(...args){
	 args = foo(args)
	 var result = soad(/* overload definition */)(args)
	 return bar(result)
}

引数と返り値もこう使うのと同じ

