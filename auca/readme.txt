おうか
auca: auto calc

input 間の自動計算


e data-auca-host
	e data-auca-group data-auca-type

Auca
  calculators
    [from, to, function] を複数
    [["type1", "type2", type1ToType2Function], ["type2", "type1", type2ToType1Function], ...]
    もしくは
    {type1: {type2: type1ToType2Function}, type2: {type1: type2ToType1Function}}
  observe_event
    input や change など

observe
  observe した要素の中の [data-auca-group] の要素に設定したタイプのイベントがおきると
  observe した要素の中の 同じ [data-auca-group] の要素全てを自動計算する
  TypeAからTypeBに自動計算するときの関数のセットが calculators

unobserve
  指定要素の observe を解除

clear
  全部の observe を解除

auto
  [data-auca-host] がついてる要素全部に observe

global
  document を監視して [data-auca-group] の要素に設定したタイプのイベントがおきると
  一番近い親の [data-auca-host] の中の同じ [data-auca-group] の要素全てを自動計算する
