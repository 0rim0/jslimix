!(function() {
	function Auca(calculators, observe_event) {
		if (typeof calculators !== "object") {
			throw new Error("First argument must be Object or Array")
		}
		if (Array.isArray(calculators)) {
			this.calculators = optimize(calculators)
		} else {
			this.calculators = calculators
		}
		this.observe_event = observe_event || "input"
		this.observing = new Map()
		this._listener = createListener(this)
	}

	Object.assign(Auca.prototype, {
		observe(elem) {
			if (!(elem instanceof HTMLElement)) throw new Error("Argument must be Element.")
			if (this.observing[elem] === undefined) return

			elem.addEventListener(this.observe_event, this._listener, false)
			this.observing[elem] = this._listener
		},
		unobserve(elem) {
			const listener = this.observing[elem]
			if (listener) {
				elem.removeEventListener(this.observe_event, listener, false)
				this.observing.delete(elem)
			}
		},
		clear() {
			for (const [elem, listener] of this.observing.entries()) {
				elem.removeListener(this.observe_event, listener, false)
				this.observing.delete(elem)
			}
		},
		auto() {
			for (const elem of document.querySelector("[data-auca-host]")) {
				this.observe(elem)
			}
		},
		global() {
			this.clear()
			document.addEventListener(this.observe_event, this._listener, false)
			this.observing[document] = this._listener
		},
	})

	function createListener(auca) {
		return function listener(eve) {
			const src = eve.target
			if (!src.matches("[data-auca-group]:not([data-auca-disabled])")) return

			const src_type = src.dataset.aucaType
			const src_group = src.dataset.aucaGroup
			const src_value = src.value

			const host =
				eve.currentTarget === document ? src.closest("[data-auca-host]") || document : eve.currentTarget

			for (const dst of host.querySelectorAll(`[data-auca-group="${src_group}"]:not([data-auca-disabled])`)) {
				if (dst === src) continue

				const dst_type = dst.dataset.aucaType
				const calculator = searchCalculator(auca.calculators, src_type, dst_type)
				if (!calculator) continue

				dst.value = calculator(src_value)
			}
		}
	}

	function optimize(arr) {
		const result = {}
		arr.forEach(([src, dst, fn]) => (result[src] ? (result[src][dst] = fn) : (result[src] = { [dst]: fn })))
		return result
	}

	function searchCalculator(calculators, src, dst) {
		var c = calculators[src] || calculators["*"]
		return c && (c[dst] || c["*"])
	}

	window.Auca = Auca
})()
