Function.prototype.overload = function overload(condition, fn) {
	const other = this
	return function overloaded(...args) {
		const new_context = this !== window
		const f = overload.match(condition, args) ? fn : other
		if (new_context) {
			return Object.setPrototypeOf(new f(...args), overloaded.prototype)
		} else {
			return f(...args)
		}
	}
}

Function.prototype.overload.match = function(c, a) {
	if (typeof c === "function") {
		return !!c(...a)
	} else if (Array.isArray(c)) {
		return c.every((e, i) => {
			if (typeof e === "string") {
				return typeof a[i] === e
			} else if (typeof e === "function") {
				return a[i] instanceof e
			} else if (e == null) {
				return true
			} else {
				return false
			}
		})
	} else {
		return match([c], a)
	}
}
