そーど
sword: softwork overload

コンパイル時に解決されるのをハード、実行時に解決するのをソフトとよんでいます


基本の使い方

関数に対して overload メソッドを呼び出し、呼び出される条件と関数を登録します
あとに登録されたものから順に自身の条件に一致するかをチェックし、一致するなら実行、一致しないなら次に移ります

var fn = (function f0(){return 0})
    .overload(["number"], function f1(){return 1})
    .overload(["number", "number"], function f2(){return 2})
    .overload(["number", "string"], function f3(){return 3})

fn に対して (1, "a") のように number, string の引数で呼び出されると 3 が返ります
(1, 2) のように number, number で呼び出されると number, string にはマッチしないので次に行き、 number, number とマッチし 2 が返ります

f1 を f2 よりあとに設定した場合は、 f2 にあてはまるものは常に f1 にマッチするので f1 が実行されます
最初の関数はすべての条件にあてはまらなかったときのデフォルト時の関数です


条件

文字列または関数の配列にします
文字列の場合は typeof 演算子、関数の場合は instanceof 演算子でマッチするかをそれぞれ判断します
配列の要素すべてにマッチすると条件を満たし、その条件とともに登録された関数が呼び出されます

また条件自体を関数にした場合は、すべての引数をその関数に渡して実行します
true が返ってくると条件を満たし、その条件とともに登録された関数が呼び出されます

Function.prototype.match を変更することで、この条件のマッチを行う処理自体を置き換えられます

