////////////////////////////////////////////////////////////////////////////////
/// Standard example

var f1 = function() {
	return "default"
}
	.overload(["number"], function(n) {
		return `number: ${n}`
	})
	.overload(["number", "string"], function(n, s) {
		return `number: ${n}, string: ${s}`
	})
	.overload([Array], function(a) {
		return ["array", a]
	})

f1()
// default
f1(10)
// number: 10
f1(3, "abc")
// number: 3, string: abc
f1([1, 2])
// ["array", [1, 2]]

////////////////////////////////////////////////////////////////////////////////
/// Use new operator

var f2 = function() {
	this.value = 0
}.overload(["number"], function(n) {
	this.value = ~~n
})
f2.prototype.triple = function() {
	return this.value * 3
}

new f2("foo")
// {value: 0}
new f2(30).triple()
// 90

////////////////////////////////////////////////////////////////////////////////
/// Use function for condition

var f3 = function() {
	return "bad"
}
	.overload(n => ~~n > 50, function() {
		return "good"
	})
	.overload(n => ~~n > 100, function() {
		return "great"
	})

f3(200)
// great
f3(20)
// bad
f3(70)
// good

////////////////////////////////////////////////////////////////////////////////
/// Custom matcher

const is = constructor => value => value && constructor && value instanceof constructor

Function.prototype.overload.match = function(c, a) {
	if (typeof c === "function") {
		return !!c(...a)
	} else if (Array.isArray(c)) {
		return c.every((e, i) => {
			if (typeof e === "string") {
				return typeof a[i] === e
			} else if (typeof e === "function") {
				return !!e(a[i])
			} else if (e == null) {
				return true
			} else {
				return false
			}
		})
	} else {
		return match([c], a)
	}
}

const f4 = function() {
	return "other"
}
	.overload([is(Object)], function() {
		return "object"
	})
	.overload([is(RegExp)], function() {
		return "regexp"
	})
	.overload([is(Date)], function() {
		return "date"
	})
	.overload([e => Array.isArray(e)], function() {
		return "array"
	})
	.overload([is(Function)], function() {
		return "function"
	})
	.overload([is((async _ => _).constructor)], function() {
		return "async function"
	})
	.overload([e => e === null], function() {
		return "null"
	})
	.overload(["undefined"], function() {
		return "undefined"
	})
	.overload(["string"], function() {
		return "string"
	})
	.overload(["boolean"], function() {
		return "boolean"
	})
	.overload(["number"], function() {
		return "double"
	})
	.overload([e => e === ~~e], function() {
		return "int32"
	})
	.overload(["symbol"], function() {
		return "symbol"
	})

f4(100)
// "int32"
f4(1.23)
// "double"
f4(false)
// "boolean"
f4("str")
// "string"
f4(Symbol())
// "symbol"
f4([1])
// "array"
f4({ a: 1 })
// "object"
f4(new Date())
// "date"
f4(/a/)
// "regexp"
f4(null)
// "null"
f4()
// "undefined"
f4(function() {})
// "function"
f4(async function() {})
// "async function"
