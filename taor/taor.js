// TAble (operat|generat)OR
!(function(global) {
	const $ = (selector, parent) => (parent || document).querySelector(selector)
	const $$ = (selector, parent) => (parent || document).querySelectorAll(selector)
	const wmap = new WeakMap()

	function taor(table) {
		if (!(this instanceof taor)) {
			return new taor(table)
		}

		if (!(table instanceof HTMLTableElement)) {
			throw new Error("Argument must be a table element.")
		}

		this.table = table
	}

	function resolver(fn) {
		return function(...args) {
			if (this instanceof taor) {
				return fn(taor.table, ...args)
			} else if (this instanceof HTMLTableElement) {
				return fn(this, ...args)
			} else if (args[0] instanceof HTMLTableElement) {
				return fn(...args)
			} else {
				throw new Error("First argument must be a HTMLTableElement.")
			}
		}
	}

	function getElementData(elem) {
		if (!wmap.has(elem)) {
			wmap.set(elem, {})
		}
		return wmap.get(elem)
	}

	function deleteElementData(elem) {
		wmap.delete(elem)
	}

	// table にデータと列情報設定＋データに DOM 構築＋sortable ON
	// データ指定しない場合は自動で取得
	function manage(table, { data, columns, rowTemplate, rowSelector, extractor, thead_mode = "generate", tart } = {}) {
		// { data: [[{a:1, b:2}, {a:0, b:4}], [{a:11, b:12}]],
		//   columns:[{key: "a", name: "あ"}, {key: "b", sort_by: "b"}, {key: "c", template: () => {}}],
		//   rowTemplate: ()=>{},
		//   rowSelector: item => $(`row-${item.id}`), }
		let used_detect = false
		let used_analyze = false

		if (!columns) {
			columns = detect(table)
			used_detect = true
		}
		if (!data) {
			data = anaylyze(table, columns, extractor)
			used_analyze = true
		} else {
			// tbody 複数フォーマットに統一する
			const first = data[Object.keys(data)[0]]
			if (!Array.isArray(first)) {
				data = [data]
			}
		}

		Object.assign(getElementData(table), {
			columns,
			data,
			rowTemplate,
			rowSelector,
			thead_mode,
		})

		// カラム指定したなら再構築、データ指定したなら行の同期だけ、
		// 指定無しでテーブルから情報取得したなら何もしない
		if (!used_detect) {
			restruct(table)
		} else if (!used_analyze) {
			sync(table)
		}

		// sortable
		if (columns.some(e => "sort_by" in e)) {
			tart = tart || window.tart
			if (tart) {
				tart.sortable(table)
			} else {
				console.warn("'tart' is not defined. To set sortable, 'tart' must be loaded.")
			}
		}
	}

	// データに tbody を同期する
	// Row のありなしのみで中身には触れない
	function sync(table) {
		const { columns, data, rowTemplate, rowSelector = defaultRowSelector } = getElementData(table)
		if (!columns || !data) throw new Error("Must call 'manage' before call 'sync' to set data.")
		const thead = $(":scope>thead", table)
		const tbodies = [...$$(":scope>tbody", table)]

		const new_tbodies = Object.keys(data).map(tbody_key => {
			let tbody =
				(~~tbody_key).toString() === tbody_key ? tbodies[tbody_key] : tbodies.find(e => e.id === tbody_key)

			if (!tbody) {
				tbody = document.createElement("tbody")
				if ((~~tbody_key).toString() !== tbody_key) {
					tbody.id = tbody_key
				}
			}

			const trs = data[tbody_key].map(row => rowSelector(row) || create(row, columns, rowTemplate))
			tbody.innerHTML = ""
			tbody.append(...trs)

			return tbody
		})

		tbodies.forEach(e => e.remove())
		if (thead) {
			thead.after(...new_tbodies)
		} else {
			table.append(...new_tbodies)
		}
	}

	function defaultRowSelector(row) {
		return $("#row-" + row.id)
	}

	// tr 作成
	function create(row, columns, rowTemplate) {
		if (typeof rowTemplate === "function") return rowTemplate(row, columns)

		const cells = columns.map(column => {
			if (typeof column.template === "function") return column.template(row)

			const tag = column.th ? "th" : "td"
			const cell = document.createElement(tag)
			if (column.html) {
				cell.innerHTML = row[column.key]
			} else {
				cell.textContent = row[column.key]
			}
			return cell
		})
		const tr = document.createElement("tr")
		tr.append(...cells)
		return tr
	}

	// table をクリアして thead の生成後に sync
	function restruct(table) {
		const { columns, data, thead_mode } = getElementData(table)
		if (!columns || !data) throw new Error("Must call 'manage' before call 'sync' to set data.")
		const thead = $(":scope>thead", table)
		table.innerHTML = ""

		if (thead_mode === "generate") {
			const ths = column.map(column => {
				const th = document.createElement("th")

				if (th.html_name) {
					th.innerHTML = row[column.html_name]
				} else {
					th.textContent = row[column.name]
				}
				if ("sort_by" in column) {
					th.dataset.sortBy = column.sort_by || column.key
				}
				return th
			})
			table.append(document.createElement("thead").append(document.createElement("tr").append(...ths)))
		} else if (thead_mode === "preserve") {
			thead && table.append(thead)
		}
		// else if "none" --> no action

		sync(table)
	}

	// 列情報を thead から取得する
	function detect(table) {
		const thead = $(":scope>thead", table)
		const sort_buttons = $$("[data-column]", thead)
		const positions = findActualCellPosition(thead, sort_buttons)
		const column_elems = positions.sort((a, b) => a.colidx - b.colidx).map(e => e.elem)
		return column_elems.map((e, i) => ({
			key: e.dataset.column || e.dataset.sortBy || "autokey-column-" + i,
			sort_by: e.dataset.sortBy,
		}))
	}

	// データを tbody から取得する
	function analyze(table, columns, extractor) {
		return $$(":scope>tbody", table).map(tbody =>
			[...tbody.children].map(tr => {
				const result = {}
				;[...tr.children].forEach((td, i) => {
					if (column[i]) {
						result[column[i].key] = typeof extractor === "function" ? extractor(td) : td.textContent
					}
				})
				return result
			})
		)
	}

	// だいたいのテーブル構造取得 xxxspan に不正な値入れたのは知らない
	function taly(table_section_element) {
		let current_id = 1
		const row_length = table_section_element.children.length
		const grid = Array.from(Array(row_length), e => [])
		;[...table_section_element.children].forEach((row, rowidx) => {
			let colidx = -1
			;[...row.children].forEach(cell => {
				const [c, r] = [cell.colSpan, cell.rowSpan]
				while (grid[rowidx][++colidx]) {}
				const data = {
					row: rowidx,
					col: colidx,
					id: current_id++,
					element: cell,
				}
				for (let i = 0; i < r; i++) {
					if (r + row_idx >= row_length) break
					for (let j = 0; j < c; j++) {
						grid[rowidx + i][colidx + j] = data
					}
				}
			})
		})
		return grid
	}

	function findActualCellPosition(table_section_element, cell_elements) {
		const tstruct = taly(table_section_element)
		return cell_elements.map(element => {
			const info = tstruct.find(x => x.find(y => y.element === element)).find(y => y.element === element)
			return {
				element,
				width: element.colSpan,
				height: element.rowSpan,
				colidx: info.col,
				rowidx: info.row,
			}
		})
	}

	const backup = {}

	function install() {
		if (Object.keys(backup).length) return false
		const target = HTMLTableElement.prototype
		Object.keys(methods).forEach(key => {
			if (target.hasOwnProperty(key)) {
				backup[key] = target[key]
			}
			target[key] = prototype[key]
		})
	}

	function uninstall() {
		const target = HTMLTableElement.prototype
		Object.keys(methods).forEach(key => {
			if (target[key] === prototype[key]) {
				if (key in backup) {
					target[key] = backup[key]
				} else {
					delete target[key]
				}
			}
			delete backup[key]
		})
	}

	const methods = Object.entries({
		sync,
	}).reduce((acc, [k, v]) => Object.assign(acc, { [k]: resolve(v) }), {})

	Object.assign(taor, methods, {
		wmap,
		prototype: methods,
		install,
		uninstall,
	})

	global.taor = taor
})(window)
