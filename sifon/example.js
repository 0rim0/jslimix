/**
mapCombination

複数配列の map 処理
1つめの引数で一番短いのに合わせるか一番長いのに合わせるか全組み合わせを行うかのモード指定
**/

mapCombination("short", (...args) => args.join("-"), [1, 2], [3, 4], [5, 6, 7])
// [
//   "1-3-5",
//   "2-4-6"
// ]

mapCombination("long", (...args) => args.join("-"), [1, 2], [3, 4], [5, 6, 7])
// [
//   "1-3-5",
//   "2-4-6",
//   "--7"
// ]

mapCombination("cross", (...args) => args.join("-"), [1, 2], [3, 4], [5, 6, 7])
// [
//   "1-3-5",
//   "1-3-6",
//   "1-3-7",
//   "1-4-5",
//   "1-4-6",
//   "1-4-7",
//   "2-3-5",
//   "2-3-6",
//   "2-3-7",
//   "2-4-5",
//   "2-4-6",
//   "2-4-7"
// ]

mapCombination("long", (...a) => a.filter(e => e !== undefined), [1, 2, 3], [4, 5, 6]).flat()
// [1, 4, 2, 5, 3, 6]


/**
condefault

条件をみたすのならば3つめの引数をそうでないなら1つめの引数を返す
条件は
  2つめの引数が関数の場合は1つめの引数を関数の引数として実行し結果がtrue
  関数でないなら1つめと2つめの引数が等しい

以下 a1 のような一時変数と条件演算子を書く手間を省ける
a2 は a1 と同等
**/
if(0){
	const a1 = function(){
		let tmp = obj.getValue().pop()
		return tmp !== "foo" ? tmp : "default"
	}()

	const a2 = condefault(obj.getValue().pop(), v => v === "foo", "default")
	// value === "foo" ==> "default"
	// value === "bar" ==> "bar"

	const a3 = condefault(stu[xyz], null, "default")
	// stu[xyz] === null ==> "default"
	// stu[xyz] === 1    ==> 1
}

condefault(100, 100, 1)
// 1

condefault(100, 200, 1)
// 100

condefault(100, x => x > 0, 1)
// 1

condefault(100, x => x < 0, 1)
// 100

