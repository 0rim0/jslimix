function mapCombination(mode, fn, ...arrays) {
	const operations = {
		short(arrs) {
			const ret = []
			for (let i = 0; true; i++) {
				if (!arrs.every(e => i in e)) break
				ret.push(fn(...arrs.map(e => e[i])))
			}
			return ret
		},
		long(arrs) {
			const ret = []
			for (let i = 0; true; i++) {
				if (!arrs.some(e => i in e)) break
				ret.push(fn(...arrs.map(e => e[i])))
			}
			return ret
		},
		cross(arrs) {
			const ret = []
			const recur = (cs, idx) => {
				if (idx in arrs) {
					arrs[idx].forEach(e => recur([...cs, e], idx + 1))
				} else {
					ret.push(fn(...cs))
				}
			}
			recur([], 0)
			return ret
		},
	}
	if (!(mode in operations)) {
		throw new Error("mode must be 'short', 'long' or 'cross'")
	}
	return operations[mode](arrays)
}

// 配列の値を交互に結合する
// alternateConcat([1,2,3], [4,5,6]) => [1,4,2,5,3,6]
function alternateConcat(...arrlikes) {
	const result = []
	const arrs = arrlikes.map(e => [...e])
	while (arrs.some(e => e.length)) {
		for (const a of arrs) {
			if (a.length) {
				result.push(a.shift())
			}
		}
	}
	return result
}

// テンプレートストリングのタグとして使う
// "`" のある行は無視
// 最初の行のインデントをベースに移行のインデントを除去する
// adjustIndent`
//     a
// `
function adjustIndent(arr, ...args) {
	const text = alternateConcat(arr, args).join("")

	const target_lines = text.split("\n").slice(1, -1)
	const indent_size = target_lines[0].match(/^[ \t]*/)[0].length
	return target_lines.map(e => e.replace(new RegExp(`^[ \t]{0,${indent_size}}`), "")).join("\n")
}

// テンプレートストリングのタグとして使う
// 1文字目がガード文字
// "`" のある行は無視
// 各行のガード文字以前は削除する
// indentGuard`#
//     #a
//     #b
// `
function indentGuard(arr, ...args) {
	const text = alternateConcat(arr, args).join("")

	const start_marker = text.charAt(0) || "|"
	return text
		.split("\n")
		.slice(1, -1)
		.map(e => e.replace(new RegExp("^[ \t]*" + regexpEscape(start_marker)), ""))
		.join("\n")
}

// 正規表現用の記号エスケープ
function regexpEscape(str) {
	const escape_char = "()[]{}.+*-\\^$".replace(/(.)/g, "\\$1")
	return str.replace(new RegExp(`([${escape_char}])`, "g"), "\\$1")
}

// text 形式で fetch
async function fetchText(...args) {
	return await fetch(...args).then(e => e.text())
}
// json 形式で fetch
async function fetchJSON(...args) {
	return await fetch(...args).then(e => e.json())
}

// 一時変数条件演算子の手間を省く
function condefault(value1, fn, value2){
	return typeof fn === "function"
		? fn(value1) ? value2 : value1
		: value1 === fn ? value2 : value1
}
