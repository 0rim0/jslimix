ぴくしー
pxy: property proxy

プロキシ機能でエラー出さずにチェーン

pxy(value, x => x.y().z.a.b())
pxy(value, x => x.v, 100)

第三引数があれば最後の結果を代入

=====
const a = {b: {c: {d: 1}}}
pxy(a, a => a.b.c.d)
// 1

pxy(a, a => a.b.c)
// {d: 1}

pxy(a, a => a.b.x.y)
// undefined

pxy(a, a => a.b.c.z, 100)
// true

pxy(a, a => a.b.s.t, 100)
// false

a
// {
//   "b": {
//     "c": {
//       "d": 1,
//       "z": 100
//     }
//   }
// }
=====

=====
const x = {}
const a = {b: function(){return x}}
pxy(a, a => a.b().y, 100)
x
// {y: 100}
=====

=====
pxy(document, d => d.querySelector("#a").innerHTML)
// undefined
pxy(document, d => d.querySelector("#a").innerHTML, "text")
// false
=====

=====
pxy(document.querySelector("table"),
    t => t.querySelectorAll("tr")[1].remove())
=====
