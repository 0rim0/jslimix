function pxy(value, process, assignment) {
	if (arguments.length < 2) {
		throw new Error("At least two arguments are required.")
	}
	const state = {
		current: value,
		parent: undefined,
		name: undefined,
	}
	const proxy = new Proxy(function() {}, {
		get(target, name, receiver) {
			state.parent = state.current
			state.name = name
			if (name in Object(state.current)) {
				state.current = state.current[name]
			} else {
				state.current = undefined
			}
			return proxy
		},
		apply(target, this_arg, args) {
			if (typeof state.current === "function") {
				state.current = state.current.apply(state.parent, args)
			} else {
				state.current = undefined
			}
			state.parent = undefined
			state.name = undefined
			return proxy
		},
	})
	process(proxy)
	if (arguments.length < 3) {
		return state.current
	}
	if (state.parent instanceof Object) {
		state.parent[state.name] = assignment
		return true
	} else {
		return false
	}
}
