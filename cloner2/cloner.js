!(function() {
	const $ = (s, r) => (r || document).querySelector(s)
	const $$ = (s, r) => (r || document).querySelectorAll(s)
	const data = {}

	// set data-* and save origin and listeners to Element.
	function init(origin, opt) {
		opt = opt || {}

		// origin
		origin = resolveElement(origin)

		if (!origin) {
			throw new Error("Missing cloner-origin.")
		}

		const uid =
			opt.id ||
			origin.dataset.clonerId ||
			Math.random()
				.toString(36)
				.substr(2)
				.substr(-12)

		const origin_parent = origin.parentElement
		origin.remove()

		// if origin is template tag, use first element child of DocumentFragment as origin.
		// if DocumentFragment has multiple element, they are wrapped by div.
		if (origin.content instanceof DocumentFragment) {
			if (origin.content.children.length > 1) {
				origin = document.createElement("div")
				origin.append(origin.content)
			} else {
				origin = origin.content.firstElementChild
			}
		}

		origin.dataset.cloner = "replica"
		origin.dataset.clonerId = uid

		// host
		const host = resolveElement(opt.host) || origin_parent
		host.dataset.cloner = "host"
		host.dataset.clonerId = uid

		// add_button
		const add_elems = resolveElements(opt.add_button)
		add_elems.forEach(add_elem => {
			add_elem.dataset.cloner = "add"
			add_elem.dataset.clonerId = uid
		})

		// delete_button
		const delete_elems = resolveElements(opt.delete_button, origin)
		delete_elems.forEach(delete_elem => {
			delete_elem.dataset.cloner = "delete"
			delete_elem.dataset.clonerId = uid
		})

		// save
		data[uid] = {
			origin,
			onbeforeadd: opt.onbeforeadd,
			onadd: opt.onadd,
			onbeforedelete: opt.onbeforedelete,
			ondelete: opt.ondelete,
		}

		return uid
	}

	function resolveElements(elem, root) {
		if (elem instanceof HTMLElement) {
			return [elem]
		}
		if (Array.isArray(elem)) {
			return elem.map(e => resolveElements(e, root)).reduce((a, b) => a.concat(b))
		}
		if (typeof elem === "string") {
			const selector = elem
			const arr = []
			root && root.matches(selector) && arr.push(root)
			return arr.concat([...$$(selector, root)])
		}

		return []
	}

	function resolveElement(elem, root) {
		if (elem instanceof HTMLElement) {
			return elem
		}

		if (typeof elem === "string") {
			const selector = elem
			return root && root.matches(selector) ? root : $(selector, root)
		}

		return null
	}

	window.addEventListener(
		"click",
		eve => {
			// click add button
			const add_button = eve.target.closest(`[data-cloner="add"]`)
			if (add_button && !add_button.hasAttribute("data-cloner-disabled")) {
				const cloner_id = add_button.dataset.clonerId
				const scope = add_button.parentElement.closest(`[data-cloner="replica"]`)
				addReplica(cloner_id, scope)
			}

			// click delete button
			const delete_button = eve.target.closest(`[data-cloner="delete"]`)
			if (delete_button && !delete_button.hasAttribute("data-cloner-disabled")) {
				const cloner_id = delete_button.dataset.clonerId
				const replica = delete_button.closest(`[data-cloner="replica"][data-cloner-id="${cloner_id}"]`)
				deleteReplica(cloner_id, replica)
			}
		},
		false
	)

	function addReplica(id, scope) {
		if (!id) return

		let host

		if (id instanceof HTMLElement) {
			host = id
			id = host && host.dataset.clonerId
		} else {
			host = $(`[data-cloner="host"][data-cloner-id="${id}"]`, scope)
		}

		if (!host || !id) return

		const opt = data[id]
		if (!opt) return

		const replica = opt.origin.cloneNode(true)

		// on before event
		let cancel = false
		opt.onbeforeadd && opt.onbeforeadd({ replica, host, cancel: _ => (cancel = true) })
		if (cancel) {
			return null
		}

		host.append(replica)

		// on after event
		opt.onadd && opt.onadd({ replica, host })

		return replica
	}

	function deleteReplica(id, replica) {
		if (!id || !replica) return

		const opt = data[id]
		if (!opt) return

		const host = replica.parentElement

		// on before event
		let cancel = false
		opt.onbeforedelete && opt.onbeforedelete({ replica, host, cancel: _ => (cancel = true) })
		if (cancel) {
			return null
		}

		replica.remove()

		// on after event
		opt.ondelete && opt.ondelete({ replica, host })

		return replica
	}

	function global(scope) {
		;[...$$(`[data-cloner="origin"]`, $(scope))].forEach(origin => init(origin))
	}

	// export
	init.data = data
	init.add = addReplica
	init.global = global
	window.cloner = init
})()
