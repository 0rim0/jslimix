くろなつー
cloner2

○ Element

template element
  クローンするテンプレート
  cloner 設定時に DOM からデタッチされる
  cloner.data に保存される

parent element
  テンプレートのクローンを追加する親 HTMLElement
  デフォルトは template element の親

add button element
  click イベント時にクローンを生成する

delete button element
  click 時にそのボタンが所属するクローンを削除


○ data-*

テンプレート
  → [data-cloner=origin]

クローンされたもの
  → [data-cloner=replica]

親要素
  → [data-cloner=host]

追加ボタン
  → [data-cloner=add]

削除ボタン
  → [data-cloner=delete]

共通()
  → [data-cloner-id=(b892a0fc31d1)]

ボタンの無効化
  → [data-cloner-disabled]


○ cloner

cloner(origin)
  テンプレートの Element を設定して初期化

cloner(origin_selector)
  セレクタでテンプレートの Element を指定して初期化

cloner(origin, opt)
  オプション付きでテンプレートの Element を指定して初期化

cloner.add(host)
  クローンした要素を追加する

cloner.add(id, scope)
  id を指定してクローンを追加する
  scope でルート要素を指定できる
  ネストすると同じ id のセットが複数存在するのでどの要素に
  クローン追加するか指定する

  cloner-parent (id:aa)
    cloner-replica (id:aa)  ← ★
      cloner-parent (id:bb)
        cloner-replica (id:bb)
        cloner-replica (id:bb)
      cloner-parent (id:bb)
        cloner-replica (id:bb)
    cloner-replica (id:aa)  ← ◆
      cloner-parent (id:bb)
        cloner-replica (id:bb)
  
  ◆ を scope にしないと cloner-parent の id (bb) の検索で
  先にくる ★ にすべて追加されてしまう

cloner.global(scope)
  すべての [data-cloner=origin] を cloner で初期化


○ オプション

origin
  HTMLElement または Selector

host
  HTMLElement または Selector

add_button
  HTMLElement(s) または Selector

delete_button
  Selector (検索するルートは [data-cloner=replica])


○ リスナ

onbeforeadd
  DOM にアタッチする前
  id をつけたりする
  キャンセルできる
  {element: HTMLElement, cancel:false}

onadd
  DOM にアタッチ後
  {element: HTMLElement}

onbeforedelete
  DOM からデタッチする前
  キャンセルできる
  {element: HTMLElement, cancel:false}

ondelete
  DOM からデタッチ後
  {element: HTMLElement}


○ cloner1 との違い

cloner と違って内部でほとんどデータをもたず data-* をメインに
使うことで動的にその時点の DOM 構造で処理できます

js で複雑な設定しないで HTML に属性だけつけて簡単に設定できます

ひとつの Element が複数の役割をもてません
クローンされたもの自体が追加・削除のボタンだったり
クローンの親要素にはなったりすることはできません

cloner-id がつくので cloner がネストしていてもだいじょうぶ
