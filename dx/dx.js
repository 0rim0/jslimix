function Dx(root, options = {}) {
	options = Object.assign({}, Dx.default_options, options)
	this.root = root
	this.options = options
	this.dxelements = Dx.build(root, options)
}

Object.assign(Dx, {
	default_options: {
		struct: "auto",
		readonly: true,
		disabled: false,
		duplicate_names: "radio",
		duplicate_group_names: "array",
		radio_group: true,
	},
	// build Dx value (object tree refers element)
	build(root, options = {}) {
		options = Object.assign({}, Dx.default_options, options)
		const targets = [...root.querySelectorAll("[data-dx-name],[data-dx-path]")]
		if (root.matches("[data-dx-name],[data-dx-path]")) {
			targets.unshift(root)
		}
		const result = new ObjectTreeEntry()
		const map = new Map()
		for (const target of targets) {
			if (target.matches("[data-dx-name][data-dx-path]")) {
				throw new Error("data-dx-name と data-dx-path の両方を指定できません: " + identify(target))
			}
			if (options.struct === "flat" && target.matches("[data-dx-path]")) {
				throw new Error("struct が flat のときは data-dx-path を指定できません: " + identify(target))
			}

			const path = detectPath(target)
			if (path.length === 0) {
				throw new Error("data-dx-path の指定が不正です: " + identify(target))
			}
			addEntry(result, path, target)
		}
		return result

		function detectPath(element) {
			const dataset = element.dataset
			if (options.struct === "flat") {
				const name = dataset.dxName.trim() || element.name.trim()
				return [{ name }]
			} else {
				const base_path = []
				route(element, root).forEach(e => {
					if (e.dataset.dxGroup) {
						base_path.unshift({ element: e, name: e.dataset.dxGroup.trim() })
					} else if (options.struct === "auto" && e.tagName === "FIELDSET") {
						base_path.unshift({ element: e, name: e.name.trim() })
					}
				})
				if ("dxName" in dataset) {
					const name = dataset.dxName.trim() || element.name.trim()
					return base_path.concat([{ name }])
				} else {
					const dxpath = dataset.dxPath.trim()
					if (dxpath.endsWith(".")) {
						throw new Error("data-dx-path は . で終わることはできません: " + identify(element))
					}
					if (dxpath.startsWith(".")) {
						const relative_path = dxpath
							.substr(1)
							.split(".")
							.map(e => ({ name: e }))
						return resolvePath(base_path.concat(relative_path))
					} else {
						const absolute_path = dxpath.split(".").map(e => ({ name: e }))
						return resolvePath(absolute_path)
					}
				}
			}
		}

		function resolvePath(path) {
			const result = []
			for (const item of path) {
				if (item.name) result.push(item)
				else result.pop()
			}
			return result
		}

		function addEntry(root_entry, path, target) {
			let entry = root_entry
			const base_path = path.slice()
			const end_name = base_path.pop().name
			// resolve group
			for (const { name, element } of base_path) {
				if (name in entry) {
					const parent_entry = entry
					entry = entry[name]
					if (entry.hasValue()) {
						throw new Error(`パスの途中にすでに値が設定されています ("${name}"): ${identify(target)}`)
					}
					if (
						options.duplicate_group_names === "merge" ||
						element === undefined ||
						map.get(entry) === element
					) {
						continue
					}
					if (options.duplicate_group_names === "throw") {
						throw new Error("data-dx-group の重複は禁止されています: " + identify(target))
					}
					// array entry
					if (map.get(entry) !== "array") {
						// create new array entry
						const array_entry = new ObjectTreeEntry()
						array_entry[0] = entry
						parent_entry[name] = array_entry
						map.set(array_entry, "array")
						entry = array_entry
					}
					const matched_entry = Object.values(entry).find(e => map.get(e) === element)
					if (matched_entry) {
						entry = matched_entry
					} else {
						entry = entry.create(entry.getNextIndex())
						map.set(entry, element)
					}
				} else {
					entry = entry.create(name)
					map.set(entry, element)
				}
			}
			// resolve name
			if (end_name in entry) {
				const end_entry = entry[end_name]
				if (end_entry.hasChildren()) {
					throw new Error(
						`パスの指定が不正です: "${end_name}" はすでに子要素をもっています: ${identify(target)}`
					)
				}
				// onduplicate: already has value
				if (options.duplicate_names === "throw") {
					throw new Error("data-dx-name の重複は禁止されています: " + identify(target))
				}
				const value = end_entry.valueOf()
				const only_radio = options.duplicate_names === "radio"
				const isRadio = elem => elem.tagName === "INPUT" && elem.type === "radio"
				if (Array.isArray(value)) {
					if (!only_radio || isRadio(target)) {
						value.push(target)
					} else {
						throw new Error("radio 以外の重複は禁止されています: " + identify(target))
					}
				} else {
					if (!only_radio || (isRadio(value) && isRadio(target))) {
						end_entry.set([value, target])
					} else {
						throw new Error("radio 以外の重複は禁止されています: " + identify(target))
					}
				}
			} else {
				entry.create(end_name, target)
			}
		}
	},
	// export form data as object without checking validity
	export(element, options) {
		return new Dx(element, options).toValue()
	},
	// restore form data from export format
	import(element, dxvalue, options) {
		const dxelements = new Dx(element, options).dxelements
		const isRadio = elem => elem.tagName === "INPUT" && elem.type === "radio"
		const getDxValue = path => {
			let value = dxvalue
			for (const name of path) {
				if (name in value) {
					value = value[name]
				} else {
					return undefined
				}
			}
			return value
		}
		const setElementValue = (element, value) => {
			if (["INPUT", "SELECT", "TEXTAREA"].includes(element.tagName)) {
				if (["radio", "checkbox"].includes(element.type)) {
					element.checked = !!value
				} else {
					element.value = value
				}
			}
		}
		dxelements.walk(
			(element, path) => {
				const value = getDxValue(path)
				if (value == null) return
				if (Array.isArray(element)) {
					if (element.every(isRadio) && options.radio_group) {
						const selected = element.find(e => e.value === String(value))
						for (const item of element) item.checked = false
						selected.checked = true
					} else {
						for (const [index, elem] of Object.entries(element)) {
							setElementValue(elem, value[index])
						}
					}
				} else {
					setElementValue(element, value)
				}
			},
			{ is_end: true, has_value: true }
		)
	},
})

Object.assign(Dx.prototype, {
	check() {
		throw new Error("not implemented")
		// ルールファイルまち
	},
	checkAndFix() {
		throw new Error("not implemented")
		// ルールファイルまち
	},
	// to DxValue
	toValue() {
		const options = this.options
		const skip = ObjectTreeEntry.undefined
		const result = this.dxelements.toPlainObject((value, path) => {
			const isRadio = elem => elem.tagName === "INPUT" && elem.type === "radio"
			const getValue = elem => {
				if (elem instanceof HTMLElement) {
					return getElementValue(elem)
				} else {
					throw new Error("Dx オブジェクトが不正です")
				}
			}
			if (Array.isArray(value)) {
				if (value.every(isRadio) && options.radio_group) {
					// radio group
					const checked_radio = value.find(e => e.checked)
					return checked_radio ? checked_radio.value : null
				} else {
					// normal group
					return value.map(getValue).filter(e => e !== skip)
				}
			} else {
				return getValue(value)
			}
		}, true)
		return new DxValue(result)

		function getElementValue(element) {
			if (element.disabled) {
				if (options.disabled === false) {
					return null
				} else if (options.disabled === "skip") {
					return skip
				}
			}
			if (element.readOnly) {
				if (options.readonly === false) {
					return null
				} else if (options.readonly === "skip") {
					return skip
				}
			}

			if ("dxValue" in element.dataset) {
				return stringAutoType(element.dataset.dxValue)
			} else if (element.tagName === "INPUT") {
				return ["radio", "checkbox"].includes(element.type) ? element.checked : element.value
			} else if (element.tagName === "SELECT" || element.tagName === "TEXTAREA") {
				return element.value
			}

			return skip
		}
	},
})

function DxValue(obj) {
	if (typeof obj === "string") {
		this.value = JSON.parse(obj)
	} else {
		this.value = obj
	}
}

Object.assign(DxValue.prototype, {
	json(indent) {
		return JSON.stringify(this.value, null, indent)
	},
	saveAs(key) {
		localStorage[key] = this.json()
	},
	sessionSaveAs(key) {
		sessionStorage[key] = this.json()
	},
	request(fetch_options) {
		const { url, method } = fetch_options
		if (!method || method === "GET" || method === "HEAD") {
			const query = "dxvalues=" + encodeURIComponent(this.json())
			const joiner = url.includes("?") ? "&" : "?"
			const query_url = url + joiner + query
			return fetch(query_url, fetch_options)
		} else {
			const body = new FormData()
			body.append("dxvalues", this.json())
			return fetch(url, Object.assign({}, fetch_options, { body }))
		}
	},
	submit(form_attributes) {
		const form = Object.assign(document.createElement("form"), {
			hidden: true,
		})
		for (const [k, v] of Object.entries(form_attributes)) {
			form.setAttribute(k, v)
		}
		const input = Object.assign(document.createElement("input"), {
			name: "dxvalues",
			value: this.json(),
		})
		form.append(input)
		document.body.append(form)
		form.submit()
		form.remove()
	},
})

// utility

function identify(elem) {
	const tag = elem.tagName.toLowerCase()
	const id = elem.id ? "#" + elem.id : ""
	const classname = Array.from(elem.classList, e => "." + e).join("")
	return [tag, id, classname].join("")
}

function route(element, ancestor) {
	const result = []
	for (let tmp = element; ; tmp = tmp.parentElement) {
		result.push(tmp)
		if (tmp === ancestor) return result
	}
	return null
}

function stringAutoType(str) {
	try {
		return JSON.parse(str)
	} catch (err) {
		return str
	}
}
