入力項目の保存・復元・送信ツール

◯機能

form など指定要素以下のデータをオブジェクト構造で保存する
json 化してテキストで取得、ローカル/セッションストレージヘ保存、フォームまたは Ajax で GET/POST リクエストができる

入力 ⇨ jsonをコピー ⇨ F5 ⇨ jsonから復元が可能

◯使い方

[data-dx-name] か [data-dx-path] ありの input/textarea/select が対象
[data-dx-value] に値を設定することで p タグや div タグなどにも値を持たせられる
[data-dx-group] にグループ名をつけることでそれ以下の要素をまとめられる

Dx インスタンスの構造サンプル
{
	a: inputelement,
	b: textareaelement,
	c: {
		d: selectelement,
		e: [radioelement, radioelement]
	},
	f: [checkelement, checkelement]
}

同名グループは「グループを配列で複数もつ」「1つのグループにまとめる」「エラーにする」から選べる
同名も「要素を配列で複数もつ」「要素すべてがradioのときのみ配列で複数もつ」「エラーにする」から選べる
通常、要素の配列は値化するときに値の配列になる
radio のみの配列の場合は値の配列(true/falseの配列)か選択されている radio の値にするか選べる

◯エクスポート（単純にオブジェクト化）

Dx.export(element, options)

element 以下の要素をオブジェクト化
オブジェクト構造は options の設定による
値のチェックなしで value そのまま

◯インポート（オブジェクトから復元）

Dx.import(element, dxvalue, options)

export と同じ element と options を指定して DOM 構造が同じなら復元される

◯Dx

new Dx(element, options)

オブジェクトを作るもとになる dxelement 要素の参照ツリー

[data-dx-group] や fieldset でネストされたオブジェクトにできる（設定でかえれる）

[data-dx-name] は階層内での名前

[data-dx-path] は 「.」 区切りのパス
構造がフラットでない場合に限り好きな場所に指定できる

「data-dx-path="a.b.c"」 はその要素の場所にかかわらず a.b.c に要素が配置される
「data-dx-path=".x.y"」 はルートからでなく本来位置するところからの相対的な場所指定
「data-dx-path=".x"」 は 「data-dx-name="x"」と同じになる

  options.struct: オブジェクト構造の設定
    flat: グループで階層化しない
    auto: data-dx-group と fieldset で階層化
    manual: data-dx-group で階層化

  options.readonly: 値化するときの設定
    true: readonly も含める
    false: readonly は null にする (radio の配列時は部分的ならないもの扱いで全部なら null)
    "skip": readonly はないもの扱いにする

  options.disabled: 値化するときの設定
    disabled プロパティを見る以外は readonly と同じ意味

  options.duplicate_names: オブジェクト構造作成時の制限
    "array": 同じ名前は配列にする
    "radio": radio 以外で同じ名前が2つ以上はゆるさない
    "throw": 同じ名前が2つ以上はゆるさない

  options.duplicate_group_names: オブジェクト構造作成時の制限
    "array": 同じ名前は配列にする (可変個数の入力処理など)
    "merge": 同じグループ名は1つにまとめる (離れた場所にある要素を1つにまとめたいとき)
    "throw": 同じグループ名が2つ以上はゆるさない

  options.radio_group: 値化するときの設定
    true: radio のみなら選択中の value を値にする (別グループの radio が混在すると一つ目の選択中の値)
    false: radio のみでも配列で取得する (boolean 型の配列になる 別グループ radio が混在するとき向け)

◯Dx メソッド

  dx.check()
    Dxオプションと値のチェックする 依存関係もみる
    okなら true
  dx.checkAndFix()
    チェックして修正可能なものは修正する
    修正不可があれば false
  dx.toValue()
    export と同じフォーマットのデータにする
    基本 value の値
    number → number型
    checkbox → boolean型
    multiple-select → 配列
    radio配列 → checked の value
    radio以外の配列 → 配列で各値
    radioとその他混合の配列 → radio も1つとして配列で各値

◯DxValue

Dx で生成したオブジェクト

new DxValue(obj)

◯DxValue メソッド

  dxvalue.json()
    json にする
  dxvalue.saveAs(name)
    localStorage に保存
  dxvalue.sessionSaveAs(name)
    sessionStorage に保存
  dxvalue.request(fetch_options)
    ajax でリクエスト
  dxvalue.submit(form_attributes)
    form で送信
