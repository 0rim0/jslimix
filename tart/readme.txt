たると
tart: table sort 


tart:
  実行方法は 3 種類
    tart.sort(table, by, asc)
    tart(table).sort(by, asc)
    tart.install();table.sort(by, asc)

install:
  tart.install で HTMLTableElement.prototype に関数追加
  tart.uninstall でもとに戻せる

data:
  tart.wmap で weakmap がとれる
  weakmap には table に対するデータが入ってる
  table をキーにして取得できる

sortable:
  DOM の値は見ないで関連付けたデータ(array of object)を元にソート

  table 内の data-sort-by 属性付き要素以下をクリックすると data-sort-by で指定した key でソートする
  data-sort-by 付き要素に data-first-order をつけて 最初の asc/desc を指定できる
  デフォルトは asc

  データは複数 tbody 対応
  配列やオブジェクトで [tbody1data, tbody2data] や {tbody1id: tbody1data, tbody2id: tbody2data}
  配列 → 順番どおり
  オブジェクト → キーが tbody の id (属性)

  ソートの値を動的に（関数呼び出し）したいなら data-sort-by に指定するキーを getter か関数にすると実行した値で比較される

stableSort:
  stableSort(array, comparer)
  比較結果が同じときは元の並びを維持する

multiSort:
  multiSort(array, condition)
  *example*
  multiSort(array, [["propname", true], [fn, false]])
  文字列ならプロパティ、自由に比較する値指定するなら関数を 1 つめの値にする
  昇順なら 2 つめの値を true

sortBy:
  sortBy(array, by)
  *example*
  sortBy(array, "propname")
  sortBy(array, ["propname", "asc"])
  sortBy(array, ["propname1", "asc"], [fn, "desc"], ["propname2"])

sortElementChildrenBy:
  sortElementChildrenBy(array, by)
  *example*
  sortElementChildrenBy(array, fn)
  sortElementChildrenBy(array, [{prop: "propname", asc: false}])
  sortElementChildrenBy(array, [{prop: "propname", asc: false}, {attr: "attrnme", asc: true}])
