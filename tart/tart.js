!(function() {
	const $ = (selector, parent) => (parent || document).querySelector(selector)
	const $$ = (selector, parent) => (parent || document).querySelectorAll(selector)
	const wmap = new WeakMap()

	function tart(table) {
		if (!(this instanceof tart)) {
			return new tart(table)
		}

		if (!(table instanceof HTMLTableElement)) {
			throw new Error("Argument must be a table element.")
		}

		this.table = table
	}

	// tbody をソートする
	function sort(by, asc) {
		const data = getElementData(this)
		const tbodies = [...this.querySelectorAll("tbody")]
		tbodies.forEach((e, i, a) => {
			const id = e.dataset.tbodyId || e.id
			id && (a[id] = e)
		})
		for (const [key, value] of Object.entries(data.data)) {
			const tbody = tbodies[key]
			if (!tbody) continue
			const sorted = stableSort(value, (a, b) => {
				const av = typeof a[by] === "function" ? a[by](a) : a[by]
				const bv = typeof b[by] === "function" ? b[by](b) : b[by]
				return av !== bv ? [-1, 1][asc ^ (av < bv)] : 0
			})
			const rows = sorted.map(data.elementSelector)
			if (rows.some(e => !(e instanceof HTMLTableRowElement))) {
				throw new Error("elementSelector cannot get HTMLTableRowElement.")
			}
			tbody.append(...rows)
		}
	}

	// tbody を反転ソートする
	function reverse() {
		const tbodies = [...this.querySelectorAll("tbody")]
		tbodies.forEach(tbody => {
			tbody.append(...[...tbody.children].reverse())
		})
	}

	// data-sort-by 属性のクリックでソートするように
	function sortable(data, elementSelector) {
		unsortable.call(this)
		const listener = onsortableclick.bind(this)
		this.addEventListener("click", listener)
		if (typeof elementSelector !== "function") {
			elementSelector = defaultElementSelector
		}
		Object.assign(getElementData(this), {
			listener,
			data: formatTableData(data),
			elementSelector,
		})
	}

	function defaultElementSelector(rowdata) {
		return $("#r" + rowdata.id)
	}

	function formatTableData(data) {
		if (Array.isArray(data)) {
			// tbody 1 つの省略系 [{},{}], 正式 [[{},{}], [{}, {}]]
			if (Array.isArray(data[0])) {
				return data
			} else if (typeof data[0] === "object") {
				return [data]
			} else {
				throw new Error("Invalid format: table data")
			}
		} else {
			if (typeof data === "object") {
				return data
			} else {
				throw new Error("Invalid format: table data")
			}
		}
	}

	// sortable の解除
	function unsortable() {
		const data = getElementData(this)
		data.listener && this.removeEventListener(data.listener)
		delete data.listener
		delete data.data
		delete data.elementSelector
	}

	function onsortableclick(eve) {
		console.assert(this instanceof HTMLTableElement)
		const elem = eve.target.closest("[data-sort-by]")
		if (!elem || !this.contains(elem)) return
		const by = elem.dataset.sortBy
		const asc = elem.dataset.sorted ? elem.dataset.sorted !== "asc" : elem.dataset.firstOrder !== "desc"
		const last_sort_elem = document.querySelector("[data-sorted]")
		if (last_sort_elem === elem) {
			reverse.call(this)
		} else {
			last_sort_elem && delete last_sort_elem.dataset.sorted
			sort.call(this, by, asc)
		}
		elem.dataset.sorted = asc ? "asc" : "desc"
	}

	function getElementData(elem) {
		if (!wmap.has(elem)) {
			wmap.set(elem, {})
		}
		return wmap.get(elem)
	}

	function clearElementData(elem) {
		wmap.delete(elem)
	}

	function sortDefault() {
		return x === y ? 0 : x < y ? -1 : 1
	}

	function formatSortCondition(cond) {
		// [["key1", true], ["key2", false], [() => Math.random(), true], ...]

		if (["function", "string"].includes(typeof cond)) {
			return [[cond, true]]
		}
		if (!Array.isArray(cond)) {
			throw new Error("Invalid sort condition.")
		}
		if (["function", "string"].includes(typeof cond[0])) {
			const asc = String(cond[1]).toLocaleLowerCase() !== "desc"
			return [[cond, asc]]
		}
		return cond.map(e => formatSortCondition(e)[0])
	}

	function formatSortElementCondition(cond) {
		// [{prop: "checked", asc: true}}, {attr: "data-value", asc: false}, ...]
		// to [[fn, true], [fn, false], ...]

		if (typeof cond === "function") {
			return [[cond, true]]
		}
		if (!Array.isArray(cond)) {
			if (typeof cond === "object") {
				const sortBy =
					"sortBy" in cond
						? cond.sortBy
						: elem =>
								"attr" in cond ? elem.getAttribute(cond.attr) : "prop" in cond ? elem[cond.prop] : null
				const asc = !!cond.asc || !("asc" in cond)
				return [[sortBy, asc]]
			}

			throw new Error("Invalid sort condition.")
		}
		return cond.map(e => formatSortElementCondition(e)[0])
	}

	function stableSortSelf(arr, fn = sortDefault) {
		const entries = Array.from(arr, (e, i) => [e, i])
		const map = new Map(entries)

		return arr.sort((x, y) => fn(x, y) || [1, -1][+(map.get(x) < map.get(y))])
	}

	function stableSort(arr, fn = sortDefault) {
		return stableSortSelf([...arr], fn)
	}

	function multiSortSelf(arr, condition) {
		return stableSortSelf(arr, (a, b) => {
			for (const [key, asc] of condition) {
				let av, bv
				if (typeof key === "function") {
					av = key(a)
					bv = key(b)
				} else {
					av = a[key]
					bv = b[key]
				}
				if (av !== bv) return [-1, 1][asc ^ (av < bv)]
			}
			return 0
		})
	}

	function multiSort(arr, condition) {
		return multiSortSelf([...arr], condition)
	}

	function sortBySelf(arr, by) {
		if (!arr) {
			throw new Error("First argument is requried.")
		}
		if (!by) {
			return arr.sort()
		}

		return multiSortSelf(arr, formatSortCondition(by))
	}

	function sortBy(arr, by) {
		if (!arr) {
			throw new Error("First argument is requried.")
		}
		if (!by) {
			return arr.slice().sort()
		}

		return multiSort(arr, formatSortCondition(by))
	}

	function sortElementChildrenBy(parent, by) {
		if (!(parent instanceof HTMLElement) || !by) {
			throw new Error("First argument and second argument are requried.")
		}

		const sorted = multiSort(parent.children, formatSortElementCondition(by))
		parent.append(...sorted)
	}

	const backup = {}

	function install() {
		if (Object.keys(backup).length) return false
		const target = HTMLTableElement.prototype
		Object.keys(prototype).forEach(key => {
			if (target.hasOwnProperty(key)) {
				backup[key] = target[key]
			}
			target[key] = prototype[key]
		})
	}

	function uninstall() {
		const target = HTMLTableElement.prototype
		Object.keys(prototype).forEach(key => {
			if (target[key] === prototype[key]) {
				if (key in backup) {
					target[key] = backup[key]
				} else {
					delete target[key]
				}
			}
			delete backup[key]
		})
	}

	function resolver(func) {
		return function(table, ...args) {
			if (this instanceof tart) {
				args.unshift(table)
				table = this.table
			} else if (this instanceof HTMLTableElement) {
				args.unshift(table)
				table = this
			} else if (!(table instanceof HTMLTableElement)) {
				throw new Error("First argument must be a table element.")
			}
			return func.apply(table, args)
		}
	}

	function objectFromEntries(entries) {
		return [...entries].reduce((a, [key, value]) => Object.assign(a, { [key]: value }), {})
	}

	const table_prototype = {
		sort,
		reverse,
		sortable,
		unsortable,
	}
	const prototype = objectFromEntries(Object.entries(table_prototype).map(([key, value]) => [key, resolver(value)]))

	window.tart = Object.assign(tart, prototype, {
		prototype,
		wmap,
		sorter: {
			stableSort,
			stableSortSelf,
			multiSort,
			multiSortSelf,
			sortBy,
			sortBySelf,
			sortElementChildrenBy,
		},
		install,
		uninstall,
	})
})()
