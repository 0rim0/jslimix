// extended-eventlistener-virtual : xelis-v

// 機能：
// 同じ関数を複数セット可能
// 優先度で実行順序を制御可能
// リスナに名前を設定可能 (削除、スキップの指定に使う。仮想リスナグループ内で重複不可)
// 実行可能回数指定可能 (once の拡張)
// cssセレクタでスコープ指定可能 (指定した要素以下以外でのイベントでは発火しない)
// 柔軟な削除が可能 (名前指定、type指定、ハンドラ指定、全部など)
// 指定したリスナのスキップが可能
//
// 制限：
// 実行順は同一条件でのみ制御可能 (passive や capture が異なる場合は実体のリスナが別なので制御不可)
//
// 仕組み：
// 登録されたリスナのハンドラは仮想的なものになって、
// 同じ条件のリスナに対して実体のリスナ1つが登録される
// 実体のリスナが仮想リスナを優先度順に呼び出す

// on({
//   type: "click",
//   handler: fn,
//   capture: false,
//   life: 1,
//   name: "",
//   priority: 0, // first, last
//   scope: ".item",
//   passive: false,
// })

!(function() {
	const original = {
		addEventListener: EventTarget.prototype.addEventListener,
		removeEventListener: EventTarget.prototype.removeEventListener,
	}

	let global_index = 0
	const listeners = new WeakMap()

	function getElementListeners(elem) {
		if (listeners.has(elem)) {
			return listeners.get(elem)
		} else {
			const new_item = {
				virtual: {},
				attached: {},
			}
			listeners.set(elem, new_item)
			return new_item
		}
	}

	function arrayReset(arr, set_arr) {
		arr.splice(0, arr.length, ...set_arr)
	}

	function arrayRemove(arr, item) {
		for (let i = 0; i < arr.length; i++) {
			if (arr[i] === item) {
				arr.splice(i--, 1)
			}
		}
	}

	function elemntToString(elem) {
		const id = elem.id ? "#" + elem.id : ""
		const classes = Array.from(elem.classList, e => "." + e).join("")
		return elem.tagName + id + classes
	}

	function makeListenerKey({ type, capture, passive }) {
		return `${type}-${!!capture}-${!!passive}`
	}

	function addListener(elem, ex_listener) {
		ex_listener.index = global_index++
		const { virtual, attached } = getElementListeners(elem)
		const key = makeListenerKey(ex_listener)
		const ex_listeners = virtual[key] || []

		// check name
		if (ex_listener.name !== null && ex_listeners.some(e => e.name === ex_listener.name)) {
			throw new Error(`duplicate name "${ex_listener.name}" at ${elemntToString(elem)}, ${key}`)
		}

		// resolve priority
		if (isNaN(+ex_listener.priority)) {
			const used_priority_range = ex_listeners.reduce(
				(a, e) => {
					return { max: Math.max(a.max, e.priority), min: Math.min(a.min, e.priority) }
				},
				{ max: 0, min: 0 }
			)
			if (ex_listener.priority === "first") {
				ex_listener.priority = used_priority_range.max + 1
			} else if (ex_listener.priority === "last") {
				ex_listener.priority = used_priority_range.min - 1
			} else {
				ex_listener.priority = 0
			}
		}

		// add to virtual and sort
		ex_listeners.push(ex_listener)
		ex_listeners.sort((a, b) => {
			if (a.priority > b.priority) return -1
			if (a.priority < b.priority) return 1
			if (a.index < b.index) return -1
			if (a.index > b.index) return 1
			return 0
		})
		virtual[key] = ex_listeners

		// make attached listener if not existing
		if (!(key in attached)) {
			const actual = createActualListener(elem, key)
			const { type, capture, passive } = ex_listener
			original.addEventListener.call(elem, type, actual, { capture, passive, once: false })
			attached[key] = actual
		}
	}

	function createActualListener(elem, key) {
		return function(eve) {
			const { virtual, attached } = getElementListeners(elem)
			const ex_listeners = virtual[key]
			for (const ex_listener of ex_listeners.slice()) {
				// check skip
				if (eve.skip_listener) {
					const skip = eve.skip_listener
					if (skip === true) {
						continue
					} else if (typeof skip === "function") {
						const do_skip = skip({
							name: ex_listener.name,
							element: elem,
							key,
						})
						if (do_skip) continue
					} else {
						const target = skip.target
						if (!(target instanceof HTMLElement) || target === elem) {
							if ("name" in skip ? skip.name === ex_listener.name : true) {
								if ("count" in skip && --skip.count <= 0) {
									eve.skip_listener = null
								}
								continue
							}
						}
					}
				}

				// check and set scope
				if (ex_listener.scope) {
					const scope_elem = eve.target.closest(ex_listener.scope)
					if (scope_elem) {
						eve.scope = scope_elem
					} else {
						continue
					}
				} else {
					eve.scope = null
				}

				// add methods
				eve.stopImmediatePropagation = eventStopImmediatePropagation
				eve.skip = eventSkip

				// call
				ex_listener.handler.call(this, eve)

				// remove listener when life is 0
				if (--ex_listener.life === 0) {
					arrayRemove(ex_listeners, ex_listener)
				}
			}
			if (eve.skip_listener && eve.skip_listener.target === "self") {
				eve.skip_listener = null
			}
		}
	}

	function eventStopImmediatePropagation() {
		this.skip_listener = true
		Event.prototype.stopImmediatePropagation.call(this)
	}

	function eventSkip(options) {
		this.skip_listener = options
	}

	function removeListener(elem, options_set) {
		const { virtual, attached } = getElementListeners(elem)

		options_set.forEach(options => {
			const ex_listeners_group = Object.entries(virtual).filter(([key]) => {
				if (!("type" in options) && !("capture" in options) && !("passive" in options)) {
					return true
				}
				const arr = key.split("-")
				const passive = arr.pop() === "true"
				const capture = arr.pop() === "true"
				const type = arr.join("-")
				if ("type" in options && options.type !== type) {
					return false
				}
				if ("capture" in options && !!options.capture !== capture) {
					return false
				}
				if ("passive" in options && !!options.passive !== passive) {
					return false
				}
				return true
			})

			for (const [key, ex_listeners] of ex_listeners_group) {
				for (const ex_listener of ex_listeners.slice()) {
					if ("handler" in options && options.handler !== ex_listener.handler) {
						continue
					}
					if ("scope" in options && options.scope !== ex_listener.scope) {
						continue
					}
					if ("name" in options && options.name !== ex_listener.name) {
						continue
					}
					arrayRemove(ex_listeners, ex_listener)
					if ("multiple" in options && !options.multiple) {
						return
					}
				}
			}
		})
	}

	Object.assign(EventTarget.prototype, {
		addEventListener(type, handler, options) {
			const { ...opt } = options && typeof options === "object" ? options : { capture: !!options }
			this.on({ ...opt, type, handler })
		},
		removeEventListener(type, handler, options) {
			if (typeof handler !== "function") throw new Error("'handler' must be a function.")
			const { ...opt } = options && typeof options === "object" ? options : { capture: !!options }
			this.off({ ...opt, type, handler })
		},
		on(options) {
			if (!options || typeof options.handler !== "function") throw new Error("'listener' must be a function.")
			const ex_listener = {
				type: String(options.type),
				handler: options.handler,
				capture: !!options.capture,
				life: ~~options.life <= 0 ? Infinity : ~~options.life,
				name: options.name ? String(options.name) : null,
				priority: options.priority,
				scope: options.scope ? String(options.scope) : null,
				passive: !!options.passive,
			}
			addListener(this, ex_listener)
		},
		off(options) {
			if (Array.isArray(options)) {
				if (options.every(e => e && typeof e === "object")) {
					removeListener(this, options)
				} else {
					throw new Error("'options' must be a object or array of object.")
				}
			} else if (options && typeof options === "object") {
				removeListener(this, [options])
			} else if (!options) {
				removeListener(this, [{}])
			} else {
				throw new Error("'options' must be a object or array of object.")
			}
		},
		getElementListeners() {
			return getElementListeners(this)
		},
	})

	EventTarget.listeners = listeners
})()
