ぜりす  
xelis: extended-eventlistener


通常版とvirtual版の2種類

機能はどちらもイベントリスナの追加削除メソッドを上書きして追加機能を使えるようにすること

通常版は設定するリスナ全部をネイティブメソッドの addEventListener でリスナ追加する  
それに対してvirtual版だと設定するリスナは仮想的なものとして扱って addEventListener ですべてにリスナを追加しない  
同じ条件のリスナに対してリスナの実体はひとつにして、そこから仮想的なリスナをすべて呼び出す

優先度機能のため通常版では追加があるごとに一度全部のリスナを解除して優先度順に並び替えた上で全部設定し直す  
virtual 版だと実体のリスナが仮想的なリスナを呼び出すときに優先度順に呼び出すだけで済む  
1 要素の同じ type にそんなに多くのリスナをつけないだろうから全付け直しでも遅いと感じることは基本ないはず

virtual 版だと実体が別になってる都合で異なる条件で設定されたリスナ間で優先度による並び替えができない  
type, capture は別のものを優先度で並び替えれるものではないので問題ないが passive には影響する  
passive の true/false を別にするとそれぞれ独立した優先度として扱われる

機能は色々あるが作った目的は「ライブラリが強制的に設定するリスナを条件によってはスキップしたい」というもの  
stopPropagation のような使い方で「次のリスナを実行しない」「以降は指定の名前のリスナを実行しない」ということができる


# バージョン

通常版


# 基本

基本は通常のメソッドと一緒

ネイティブメソッドだと思ってライブラリが呼び出しても大丈夫になってる

```js
const fn = e => console.log(1)
document.body.addEventListener("click", fn)
document.body.dispatchEvent(new Event("click"))
// 1
document.body.removeEventListener("click", fn)
document.body.dispatchEvent(new Event("click"))
// Nothing happens
```

# 優先度

priority プロパティが大きいほど優先される

```js
window.addEventListener("click", e => console.log(1))
window.addEventListener("click", e => console.log(2))
window.dispatchEvent(new Event("click"))
// 1
// 2
window.addEventListener("click", e => console.log(3), {priority: 10})
window.dispatchEvent(new Event("click"))
// 3
// 1
// 2
window.addEventListener("click", e => console.log(4), {priority: -1})
window.dispatchEvent(new Event("click"))
// 3
// 1
// 2
// 4
window.addEventListener("click", e => console.log(5))
window.dispatchEvent(new Event("click"))
// 3
// 1
// 2
// 5
// 4
```

`first` と `last` を設定すると自動で最初または最後になるよう priority を設定する

```js
window.addEventListener("click", e => console.log("base"))
window.addEventListener("click", e => console.log("before3"), {priority: "first"})
window.addEventListener("click", e => console.log("after1"), {priority: "last"})
window.addEventListener("click", e => console.log("after2"), {priority: "last"})
window.addEventListener("click", e => console.log("before2"), {priority: "first"})
window.addEventListener("click", e => console.log("before1"), {priority: "first"})
window.dispatchEvent(new Event("click"))
// before1
// before2
// before3
// base
// after1
// after2
```

# 削除

name の指定

```js
window.on({type: "click", handler: e => console.log(1), name: "foo"})
window.on({type: "click", handler: e => console.log(2), name: "bar"})
window.off({name: "foo"})
window.dispatchEvent(new Event("click"))
// 2
```

全部削除

```js
window.on({type: "click", handler: e => console.log(1), name: "foo"})
window.on({type: "click", handler: e => console.log(2), name: "bar"})
window.off()
window.dispatchEvent(new Event("click"))
// Nothing happens
```

複数条件

```js
window.on({type: "click", handler: e => console.log(1), name: "foo"})
window.on({type: "click", handler: e => console.log(2), name: "bar"})
window.off([{name: "foo"}, {name: "bar"}])
window.dispatchEvent(new Event("click"))
// Nothing happens
```

同一条件の複数削除

multiple を false にするとその条件に対してそれ以上の削除をしない  
multiple のデフォルトは true

```js
window.on({type: "click", handler: e => console.log(1), name: "foo"})
window.on({type: "click", handler: e => console.log(2), name: "bar"})
window.off({type: "click", multiple: true})
window.dispatchEvent(new Event("click"))
// Nothing happens

window.off()

window.on({type: "click", handler: e => console.log(1), name: "foo"})
window.on({type: "click", handler: e => console.log(2), name: "bar"})
window.off({type: "click", multiple: false})
window.dispatchEvent(new Event("click"))
// 2
```

# スコープ

```js
window.addEventListener("click", e => console.log(e.scope), {scope: "button"})
document.body.innerHTML = `
	<p>text</p>
	<button>
		button
		<span>span</span>
	</button>
`
document.querySelector("p").dispatchEvent(new Event("click", {bubbles: true}))
// Nothing happens
document.querySelector("button").dispatchEvent(new Event("click", {bubbles: true}))
// <button>​…​</button>​
document.querySelector("span").dispatchEvent(new Event("click", {bubbles: true}))
// <button>​…​</button>​
```

# ライフ

```js
window.on({type: "click", handler: e => console.log(1), life: 1})
window.on({type: "click", handler: e => console.log(2), life: 3})
window.dispatchEvent(new Event("click"))
// 1
// 2
window.dispatchEvent(new Event("click"))
// 2
window.dispatchEvent(new Event("click"))
// 2
window.dispatchEvent(new Event("click"))
// Nothing happens
```

# スキップ

```js
window.addEventListener("click", e => {
	console.log(1)
	e.skip(({name}) => name === "listener2")
}, {name: "listener1"})
window.addEventListener("click", e => {
	console.log(2)
}, {name: "listener2"})
window.addEventListener("click", e => {
	console.log(3)
}, {name: "listener3"})
window.dispatchEvent(new Event("click"))
// 1
// 3
```

not virtual 版ではスキップにはコールバック関数のみしか渡せない

virtual では 1 リスナ内で仮想リスナ全部処理する都合上、使い方も異なる

- stopImmediatePropagation の機能をスキップで実装してるので true 指定で全スキップ機能がある
- 同一リスナの仮想リスナのみスキップするためにオブジェクト形式で skip.target の指定ができる


