// extended-eventlistener : xelis

// 機能：
// 同じ関数を複数セット可能
// 優先度で実行順序を制御可能
// リスナに名前を設定可能 (削除、スキップの指定に使う。仮想リスナグループ内で重複不可)
// 実行可能回数指定可能 (once の拡張)
// cssセレクタでスコープ指定可能 (指定した要素以下以外でのイベントでは発火しない)
// 柔軟な削除が可能 (名前指定、type指定、ハンドラ指定、全部など)
// 指定したリスナのスキップが可能
//
// 仕組み：
// 登録削除のメソッドを上書きしてリスナを管理するようにしている
// 追加時にはその type のメソッド全部を再登録することで優先度順に呼び出されるようにしている

// on({
//   type: "click",
//   handler: fn,
//   capture: false,
//   life: 1,
//   name: "",
//   priority: 0, // first, last
//   scope: ".item",
//   passive: false,
// })

!(function() {
	const original = {
		addEventListener: EventTarget.prototype.addEventListener,
		removeEventListener: EventTarget.prototype.removeEventListener,
	}

	let global_index = 0
	const listeners = new WeakMap()

	function getElementListeners(elem) {
		if (listeners.has(elem)) {
			return listeners.get(elem)
		} else {
			const new_item = {}
			listeners.set(elem, new_item)
			return new_item
		}
	}

	function arrayReset(arr, set_arr) {
		arr.splice(0, arr.length, ...set_arr)
	}

	function arrayRemove(arr, item) {
		for (let i = 0; i < arr.length; i++) {
			if (arr[i] === item) {
				arr.splice(i--, 1)
			}
		}
	}

	function elemntToString(elem) {
		const id = elem.id ? "#" + elem.id : ""
		const classes = Array.from(elem.classList, e => "." + e).join("")
		return elem.tagName + id + classes
	}

	function addListener(elem, ex_listener) {
		ex_listener.index = global_index++
		const listeners = getElementListeners(elem)
		const type = ex_listener.type
		const ex_listeners = listeners[type] || []

		// check name
		if (ex_listener.name !== null && ex_listeners.some(e => e.name === ex_listener.name)) {
			throw new Error(`duplicate name "${ex_listener.name}" at ${elemntToString(elem)}, ${type}`)
		}

		// resolve priority
		if (isNaN(+ex_listener.priority)) {
			const used_priority_range = ex_listeners.reduce(
				(a, e) => {
					return { max: Math.max(a.max, e.priority), min: Math.min(a.min, e.priority) }
				},
				{ max: 0, min: 0 }
			)
			if (ex_listener.priority === "first") {
				ex_listener.priority = used_priority_range.max + 1
			} else if (ex_listener.priority === "last") {
				ex_listener.priority = used_priority_range.min - 1
			} else {
				ex_listener.priority = 0
			}
		}

		// re-add all listeners of type
		for (const added_listener of ex_listeners) {
			original.removeEventListener.call(elem, type, added_listener.wrapper, added_listener)
		}
		ex_listener.wrapper = createHandlerWrapper(ex_listener, elem, type)
		ex_listeners.push(ex_listener)
		ex_listeners.sort((a, b) => {
			if (a.priority > b.priority) return -1
			if (a.priority < b.priority) return 1
			if (a.index < b.index) return -1
			if (a.index > b.index) return 1
			return 0
		})
		for (const add_listener of ex_listeners) {
			original.addEventListener.call(elem, type, add_listener.wrapper, add_listener)
		}
		listeners[type] = ex_listeners
	}

	function createHandlerWrapper(ex_listener, elem, type) {
		return function(eve) {
			const listeners = getElementListeners(elem)
			const ex_listeners = listeners[type]

			// check skip
			if (eve.checkSkip && checkSkip({ ex_listener, event: eve })) {
				return
			}

			// check and set scope
			if (ex_listener.scope) {
				const scope_elem = eve.target.closest(ex_listener.scope)
				if (scope_elem) {
					eve.scope = scope_elem
				} else {
					return
				}
			} else {
				eve.scope = null
			}

			// add methods
			eve.skip = eventSkip

			// call
			ex_listener.handler.call(this, eve)

			// remove listener when life is 0
			if (--ex_listener.life === 0) {
				arrayRemove(ex_listeners, ex_listener)
				original.removeEventListener.call(elem, type, ex_listener.wrapper, ex_listener)
			}
		}
	}

	function eventSkip(checkSkip) {
		if (typeof checkSkip !== "function") throw new Error("Skip condition must be a function.")
		this.checkSkip = checkSkip
	}

	function removeListener(elem, options_set) {
		const listeners = getElementListeners(elem)

		options_set.forEach(options => {
			const ex_listeners_group = Object.entries(listeners).filter(([type]) => {
				if ("type" in options) {
					return options.type === type
				} else {
					return true
				}
			})

			for (const [type, ex_listeners] of ex_listeners_group) {
				for (const ex_listener of ex_listeners.slice()) {
					if ("capture" in options && options.capture !== ex_listener.capture) {
						continue
					}
					if ("passive" in options && options.passive !== ex_listener.passive) {
						continue
					}
					if ("handler" in options && options.handler !== ex_listener.handler) {
						continue
					}
					if ("scope" in options && options.scope !== ex_listener.scope) {
						continue
					}
					if ("name" in options && options.name !== ex_listener.name) {
						continue
					}
					arrayRemove(ex_listeners, ex_listener)
					original.removeEventListener.call(elem, type, ex_listener.wrapper, ex_listener)
					if ("multiple" in options && !options.multiple) {
						return
					}
				}
			}
		})
	}

	Object.assign(EventTarget.prototype, {
		addEventListener(type, handler, options) {
			const { ...opt } = options && typeof options === "object" ? options : { capture: !!options }
			this.on({ ...opt, type, handler })
		},
		removeEventListener(type, handler, options) {
			if (typeof handler !== "function") throw new Error("'handler' must be a function.")
			const { ...opt } = options && typeof options === "object" ? options : { capture: !!options }
			this.off({ ...opt, type, handler })
		},
		on(options) {
			if (!options || typeof options.handler !== "function") throw new Error("'handler' must be a function.")
			const ex_listener = {
				type: String(options.type),
				handler: options.handler,
				capture: !!options.capture,
				life: ~~options.life <= 0 ? Infinity : ~~options.life,
				name: options.name ? String(options.name) : null,
				priority: options.priority,
				scope: options.scope ? String(options.scope) : null,
				passive: !!options.passive,
			}
			addListener(this, ex_listener)
		},
		off(options) {
			if (Array.isArray(options)) {
				if (options.every(e => e && typeof e === "object")) {
					removeListener(this, options)
				} else {
					throw new Error("'options' must be a object or array of object.")
				}
			} else if (options && typeof options === "object") {
				removeListener(this, [options])
			} else if (!options) {
				removeListener(this, [{}])
			} else {
				throw new Error("'options' must be a object or array of object.")
			}
		},
		getElementListeners() {
			return getElementListeners(this)
		},
	})

	EventTarget.listeners = listeners
})()
