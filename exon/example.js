/*****
Array flat
*****/

;[1, [2, 3], [4, [5, [[[6]], 7], 8], 9]].flat()
// [1, 2, 3, 4, [5, [[[6]], 7], 8], 9]
;[1, [2, 3], [4, [5, [[[6]], 7], 8], 9]].flat().flat()
// [1, 2, 3, 4, 5, [[[6]], 7], 8, 9]
;[1, [2, 3], [4, [5, [[[6]], 7], 8], 9]].flatRecursive()
// [1, 2, 3, 4, 5, 6, 7, 8, 9]
;[1, [2, 3], [4, [5, [[[6]], 7], 8], 9]].flatRecursive(2)
// [1, 2, 3, 4, 5, [[[6]], 7], 8, 9]

const a = [1, 2]
const b = [3, 4, a]
a.push(b)
a.flatRecursive()
// Error: 循環参照がみつかりました
a.flatRecursive(null, true)
// Error: 循環参照を許容する場合は繰り返し上限を指定する必要があります
a.flatRecursive(5, true)
// [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, Array(3)]

/*****
Table growCell

table の data-column に指定したカラム数になるように
data-grow 付きのセルの colspan を増やす
data-grow が大きいほど優先される
*****/

document.body.innerHTML = `
<table data-column="3">
    <tbody>
        <tr><td>1</td><td>1</td></tr>
        <tr><td data-grow="1">2</td></tr>
        <tr><td>3</td><th data-grow="1">2</th></tr>
        <tr><td data-grow="1">4</td><td>3</td><td>1</td></tr>
        <tr><th data-grow="1">5</th><th>4</th><th>2</th><th>3</th></tr>
    </tbody>
</table>
<style>
    td, th { background: yellowgreen; }
</style>
`
document.querySelector("table").growCell()

// <table data-column="3">
//     <tbody>
//         <tr><td>1</td><td>1</td></tr>
//         <tr><td data-grow="1" colspan="3">2</td></tr>
//         <tr><td>3</td><th data-grow="1" colspan="2">2</th></tr>
//         <tr><td data-grow="1">4</td><td>3</td><td>1</td></tr>
//         <tr><th data-grow="1">5</th><th>4</th><th>2</th><th>3</th></tr>
//     </tbody>
// </table>

document.body.innerHTML = `
<table data-column="100">
    <tr><td>1</td><td data-grow="1">2</td></tr>
    <tr><td data-grow="3">3</td><td data-grow="5">4</td><td data-grow="2">5</td></tr>
    <tr><td data-grow="1">6</td><th data-grow="1">7</th></tr>
</table>
`
document.querySelector("table").growCell()

// <table data-column="100">
//     <tbody><tr><td>1</td><td data-grow="1" colspan="99">2</td></tr>
//     <tr><td data-grow="3" colspan="31">3</td><td data-grow="5" colspan="48">4</td><td data-grow="2" colspan="21">5</td></tr>
//     <tr><td data-grow="1" colspan="50">6</td><th data-grow="1" colspan="50">7</th></tr>
// </tbody></table>
