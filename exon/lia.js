function limitArguments(fn, num, bind_this) {
	return (...args) => fn.apply(bind_this, args.slice(0, ~~num))
}

Function.prototype.lia = function(num, bind_this) {
	return (...args) => this.apply(bind_this, args.slice(0, ~~num))
}
