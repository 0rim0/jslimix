かりす  
callise: call promise

`Proxy.left`, `Proxy.right` に関数を渡すと、その最左または最右の引数をコールバック関数みなして実行時に解決される Promise を返す関数にする  
`Function.prototype.la`, `Function.prototype.ra` を使ってメソッドって更に短くかける  
`setTimeout.la(3000)` など

```js
function sleepL(callback, time) {
	const start = Date.now()
	setTimeout(_ => {
		callback(Date.now() - start)
	}, time)
}

function sleepR(time, callback) {
	const start = Date.now()
	setTimeout(_ => {
		callback(Date.now() - start)
	}, time)
}

!(async function() {
	console.time("1000ms")
	const actual_wait1 = await sleepL.la(1000)
	console.timeEnd("1000ms")
	console.log(actual_wait1)

	console.time("2000ms")
	const actual_wait2 = await sleepR.ra(2000)
	console.timeEnd("2000ms")
	console.log(actual_wait2)

	console.time("foo")
	const value = await Promise.left(setTimeout)(1000, "foo")
	console.timeEnd("foo")
	console.log(value)
})()
```

```
1000ms: 1.00e+3ms
1001
2000ms: 2.00e+3ms
2002
foo: 1.00e+3ms
foo
```

In chrome extension

```js
!(async function() {
	const tabs = await chrome.tabs.query.ra({})
	console.log(tab)
})()
```
