// method
Object.assign(Array.prototype, {
	// 配列のレベルを 1 段減らす
	// strict に true を指定すると、配列の中に配列以外の要素があるとエラーになる
	// [1, [2], [[3, [4]]]].flat() => [1, 2, [3, [4]]]
	// [1, [2], [[3, [4]]]].flat(true) => 1 が配列ではないのでエラー
	flat(strict = false) {
		return this.reduce((acc, e) => {
			if (Array.isArray(e)) {
				return acc.concat(e)
			} else if (strict) {
				throw new Error("Not an array is Found.")
			} else {
				return acc.concat([e])
			}
		}, [])
	},
	// flat を繰り返す
	// 配列の中に配列がなくなるか繰り返し最大数を超えると終了
	flatRecursive(max, allow_cyclic_reference = false) {
		max = ~~max <= 0 ? Infinity : ~~max - 1
		if (max === Infinity && allow_cyclic_reference) {
			throw new Error("循環参照を許容する場合は繰り返し上限を指定する必要があります")
		}
		const set = new Set()
		return (function recur(arr, limit) {
			// 循環参照のチェック
			if (!allow_cyclic_reference === false) {
				for (const a of arr.filter(Array.isArray)) {
					if (set.has(a)) {
						throw new Error("循環参照がみつかりました")
					}
					set.add(a)
				}
			}
			const flat_arr = arr.flat(false)
			if (flat_arr.some(Array.isArray) && limit) {
				return recur(flat_arr, limit - 1)
			} else {
				return flat_arr
			}
		})(this, max)
	},
	// 配列の指定したインデックスの値を削除する
	// 配列でインデックスを複数指定できる
	// 破壊的
	// 削除された値を返す (引数が配列なら返り値も配列形式)
	removeIndex(n) {
		const remove_multiple = Array.isArray(n)
		const indexes = remove_multiple ? [...n] : [n]
		const removed = []
		for (const [seq, index] of indexes) {
			removed.push(...this.splice(seq - index, 1))
		}
		return remove_multiple ? removed : removed[0]
	},
	// removeIndex の非破壊的版
	// 返り値は削除後の配列
	// 削除された値は取得できない
	removeIndexNew(n) {
		const copy = this.slice()
		copy.removeIndex(n)
		return copy
	},
	// 引数に === で一致する要素を削除する
	// all を true にすれば一致するすべて、false にすれば最初の 1 つを削除する
	// 破壊的
	// 元配列の削除されたインデックスを配列で返す
	// (callback で条件指定するなら filter を使えばいいのでcallbackタイプは作らない)
	remove(item, all = true) {
		// ループ中に length が更新されるので毎回取得
		const indexes = []
		for (let i = 0; i < this.length; ) {
			if (this[i] === item) {
				this.splice(i, 1)
				indexes.push(i)
				if (!all) {
					return i
				}
			} else {
				i++
			}
		}
		// それまでの要素削除後のインデックスなのでN番目分を足す
		return indexes.map((e, i) => e + i)
	},
	// remove の非破壊的版
	// 返り値は削除後の配列
	// 削除された値は取得できない
	removeNew(item, all = true) {
		const copy = this.slice()
		copy.remove(item, all)
		return copy
	},
})

// property
Object.defineProperties(Array.prototype, {
	first: {
		get() {
			return this[0]
		},
		set(value) {
			return this.unshift(value)
		},
	},
	last: {
		get() {
			return this[this.length - 1]
		},
		set(value) {
			return this.push(value)
		},
	},
})
