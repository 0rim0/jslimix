{
	HTMLTableElement.prototype.growCell = function() {
		const col = ~~this.dataset.column
		if (col <= 0) {
			throw new Error("Table must have data-column attribute.")
		}
		for (const tr of this.querySelectorAll(":scope>*>tr")) {
			const cells = [...tr.querySelectorAll(":scope>td,:scope>th")]
			const len = cells.reduce((a, e) => a + e.colSpan, 0)
			const rem = col - len
			if (rem <= 0 || len === 0) continue
			const grow_elements = []
			let grow_level = cells.map(e => ({ element: e, level: ~~e.dataset.grow })).filter(e => e.level)
			while ((grow_level = grow_level.filter(e => e.level)).length) {
				for (const item of grow_level) {
					grow_elements.push(item.element)
					item.level--
				}
			}
			if (!grow_elements.length) continue
			for (const _ of Array(rem)) {
				const grow = grow_elements.shift()
				grow.colSpan++
				grow_elements.push(grow)
			}
		}
	}
}
