りあ
lia: limit arguments

関数が受け取る引数を制限する

map のコールバックに parseInt を渡すとコールバックに 2 つめの引数にインデックスが渡されて基数が変わってしまう  
こういうことを防ぐ

```js
function addAll(...args){ return args.reduce((acc, e) => acc + e, 0) }

addAll.lia(3)(1,2,3,4)
// 6

limitArguments(addAll, 1)(1,2,3,4)
// 1
```

```js
;[65,66,67].map(String.fromCharCode)
// ["A  ", "B□ ", "C□ "]

;[65,66,67].map(String.fromCharCode.lia(1))
// ["A", "B", "C"]
```

```js
"1,2,3,4".split(",").map(parseInt)
// [1, NaN, NaN, NaN]

"1,2,3,4".split(",").map(parseInt.lia(1))
// [1, 2, 3, 4]
```

```js
var obj = {
	a: 1,
	f: function(x){return this.a + x}
}
;[1,2,3].map(obj.f.lia(1, obj))
// [2, 3, 4]

;[1,2,3].map(obj.f.lia(1))
// [undefined, undefined, undefined]
```


