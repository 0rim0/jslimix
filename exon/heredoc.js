Function.prototype.hereDoc = function(adjust_indent) {
	var text = this.toString()
	var match = text.match(/^function \(\)\{\/\*([^]*)\*\/\}$/)
	if (!match) return null

	var here_text = match[1]
	if (!detect_indent) return here_text

	var first_pos = here_text.indexOf("\n") + 1
	var last_pos = here_text.lastIndexOf("\n")
	var target_text = here_text.slice(first_pos, last_pos)

	var indent_size = target_text.match(/^[ \t]*/)[0].length
	return target_text
		.split("\n")
		.map(function(e) {
			return e.replace(new RegExp("^[ \t]{0," + indent_size + "}"), "")
		})
		.join("\n")
}
