!(function() {
	Promise.left = function(fn) {
		return (...args) =>
			new Promise(resolve => {
				fn(resolve, ...args)
			})
	}

	Promise.right = function(fn) {
		return (...args) =>
			new Promise(resolve => {
				const padded_args = Object.assign(Array(Math.max(0, fn.length - 1)), args)
				fn(...args, resolve)
			})
	}

	Function.prototype.la = function(...args) {
		return Promise.left(this)(...args)
	}

	Function.prototype.ra = function(...args) {
		return Promise.right(this)(...args)
	}
})()
